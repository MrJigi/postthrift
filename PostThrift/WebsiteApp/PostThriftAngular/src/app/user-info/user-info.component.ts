import { Component, OnInit } from '@angular/core';
import {DataService } from '../data.service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.css'],
  providers: [DataService]
})
export class UserInfoComponent implements OnInit {
  productId: any;
  productData: any;

  constructor(
   // private crudService: CrudService,
    private router: Router,
    private actRoute: ActivatedRoute) { }


  // tslint:disable-next-line:typedef
  ngOnInit() {
  this.productId = this.actRoute.snapshot.params.id;
  }

  // InfoReceived1: string[]=[];
  // InfoReceived2: string[]=[];
  // InfoReceived3: string[]=[];

  // getInfoFromService1(){
  //   this.InfoReceived1 = this.dservice.getInfo1()
  // }
  // getInfoFromService2(){
  //   this.InfoReceived2 = this.dservice.getInfo2()
  // }
  // getInfoFromService3(){
  //   this.InfoReceived3 = this.dservice.getInfo3()
  // }




  // updateInfo(frm: any){
  //   this.dservice.addInfo(frm.value.location)
  // }

}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentPageFeaturesComponent } from './component-page-features.component';

describe('ComponentPageFeaturesComponent', () => {
  let component: ComponentPageFeaturesComponent;
  let fixture: ComponentFixture<ComponentPageFeaturesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentPageFeaturesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentPageFeaturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

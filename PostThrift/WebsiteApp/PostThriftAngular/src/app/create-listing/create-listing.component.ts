import { Component, OnInit } from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {DataService} from "../data.service";
import {Product} from "../product";
import {listLazyRoutes} from "@angular/compiler/src/aot/lazy_routes";
import {AuthentificationService} from "../authentification.service";
import {Member} from "../member";
import {AdvertiseRequest} from "../advertiseRequest";
import {Router} from "@angular/router";

@Component({
  selector: 'app-create-listing',
  templateUrl: './create-listing.component.html',
  styleUrls: ['./create-listing.component.css']
})
export class CreateListingComponent implements OnInit{

  categoryList: string[] = [] ;
  selectedCategory: string = '';
  user;
  member ;
  private checkUserUsername: string;


  constructor(
    private service: DataService,
    private authService: AuthentificationService,
    private router: Router

  ) {
    this.authService.user.subscribe( response => this.user)}
  ngOnInit() {
    this.allCategory();
    this.getCurrentUser();
  }
 public onCategorySelect(category: string) {
    this.selectedCategory = category;
 }
  public allCategory() {
    this.service.getCategories().subscribe(
      response => {
      this.categoryList = response;
      this.selectedCategory = this.categoryList[0];
    },(error: HttpErrorResponse) => {
      alert(error.message);
    });
  }

  getCurrentUser() :void {
    const memberIdentity : number = this.authService.getUser.id;
    this.member = this.service.getMemberById(memberIdentity).subscribe(
      (response: any) =>{
        this.member = response;
      },
      (error: HttpErrorResponse) => {
        alert("Couldn't get the member error :" + error.message);
      }
    );

  }
  getUserUsername() : any {
      return this.member.username;
  }


  public onSubmit(Form: any ){
    console.log("new item is being submitted");
    let soldProduct: Product;
    soldProduct = {

      name: Form.value.name,
      seller: this.getUserUsername(),
      price: Form.value.price,
      description: Form.value.description,
      city: Form.value.city,
      category: this.selectedCategory,

    } as Product;

    let advert: AdvertiseRequest;
    advert = {
      sellerUsername: this.getUserUsername(),
      product: soldProduct
    };
    console.log(soldProduct);
    this.service.createProduct(advert).subscribe(
      (response  ) => {

        alert("new item has been submitted");
        this.router.navigate(['//GetUserProfile/', this.member.id]);
    },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }


    );

  }

}

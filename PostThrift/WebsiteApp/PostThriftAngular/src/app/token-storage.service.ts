import { Injectable } from '@angular/core';
import {JwtResponse} from "./JwtResponse";

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  private USER_KEY = "User-Key";

  constructor() { }

  public clearUser() : void {
    localStorage.removeItem(this.USER_KEY);
  }
  public getUser() : JwtResponse | null {
    const user = localStorage.getItem(this.USER_KEY);
    if(user !== null && user !== undefined && user !== 'undefined'){
      const userObject = JSON.parse(user);
      return new JwtResponse(userObject.token,userObject.type,userObject.id,userObject.roles);
    }
    else{
      return null;
    }
  }

  public saveUser(user : JwtResponse) : void {
    localStorage.removeItem(this.USER_KEY);
    if(user){
      localStorage.setItem(this.USER_KEY,JSON.stringify(user));
    }
  }
}



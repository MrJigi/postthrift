export class Notification{
public constructor(public text: string,
                   public sender: string,
                   public time: string,
                   public messageId: string)
{
}


}

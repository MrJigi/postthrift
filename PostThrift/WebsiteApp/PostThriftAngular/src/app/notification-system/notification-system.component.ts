import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebSocketsService} from "../web-sockets.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {Message} from "../message";
import {NotificationChat} from "../notificationChat";
import {DataService} from "../data.service";
import {AuthentificationService} from "../authentification.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Product} from "../product";
import {HttpErrorResponse} from "@angular/common/http";
import {Subscription} from "rxjs";
import {Member} from "../member";

// import * as SockJS from 'sockjs-client';
// import * as StompJs from '@stomp/stompjs';
// import {Stomp} from "@stomp/stompjs";
// import {$} from "protractor";
// import {elementAt} from "rxjs/operators";
// import {WebSocketsService} from "../web-sockets.service";


@Component({
  selector: 'app-notification-system',
  templateUrl: './notification-system.component.html',
  styleUrls: ['./notification-system.component.css']
})
export class NotificationSystemComponent implements OnInit, OnDestroy { // extends WebSocketsService  Doesn't work well


  // websocketService ;
  //
  // constructor(webSocket : WebSocketsService) {
  //   this.websocketService = webSocket;
  // }
  notificationSub:Subscription;
  members: Member[] = [];
  messages: Message[] = [];
  selectedMember: Member = null;
  chatForm: FormGroup;
  loggedInUsername = '';


  constructor(private webSocketService: WebSocketsService,
              private dataService:DataService,
              private formBuilder: FormBuilder,
              private authService: AuthentificationService,
              private router :Router,
              private route: ActivatedRoute) {
    if (this.authService.getUser !== null && this.authService.getUser !== undefined){
      this.notificationSub = this.webSocketService.notifications.subscribe((newNotifications) => {
        if(newNotifications != undefined && newNotifications.length > 0)
        {
          const lastNotification = newNotifications[newNotifications.length -1];
          this.handleNotification(lastNotification);
        }
        this.dataService.getMemberById(this.authService.getUser.id).subscribe(
          data => {
            this.loggedInUsername = data.username;
          }
        );
        console.log(this.loggedInUsername);
      });
      this.chatForm = this.formBuilder.group({
        inputBox : new FormControl('', [Validators.required])
      });
    }else{
      this.router.navigate(['/LogInComponent']);
    }
  }

 handleNotification(notification: NotificationChat) {
    if(this.selectedMember.username === notification.sender){
      this.dataService.getMessageById(notification.messageId).subscribe( data => {
        if(this.messages === null || this.messages === undefined){
          this.messages = [];
        }
        this.messages.push(data);
      }, error => {
        alert("notification not received");
      });

    }
 }

 getChatMessagesWithSeller(sellerName: string){
    this.dataService.getAllMessages(this.authService.getUser.id, sellerName).subscribe(messages =>{
      this.messages = messages;
    },
      error => {
      this.messages = [];
      });
 }

  checkIfAdvertIsSelected(sellerName: string): boolean{
    if (this.selectedMember == null){
      return false;
    }
    else{
      return this.selectedMember.username === sellerName;
    }
  }

  handleAdvertSelect(member: Member): void{
    if ((this.selectedMember != null) && (member.username === this.selectedMember.username)){
      this.selectedMember = null;

      console.log(this.selectedMember);
    }
    else{
      this.selectedMember = member;
      console.log(this.selectedMember);
      this.getChatMessagesWithSeller(this.selectedMember.username);
    }
  }
  // connectUser(){
  //
  // }

  // connectClicked(){
  //   if (!this.webSocketService.connectionStatus){
  //     this.webSocketService.onConnect();
  //   }
  //
  //   this.webSocketService.notifications.subscribe((newNotifications) => {
  //     this.notifications = newNotifications;
  //   });
  // }

  disconnectClicked(){
    this.webSocketService.disconnect();
  }

  sendChatMessage(){
    if (this.chatForm.controls.inputBox.value !== ''){
      console.log("text box is not empty");

      if(this.loggedInUsername != null && this.loggedInUsername !== ''){
        let message: Message = new Message(null,
          null,
          null,
          this.loggedInUsername,
          null,
          this.selectedMember.username,
          this.chatForm.controls.inputBox.value,
          null,null);
        message.messageTimeStamp = new Date();
        this.messages.push(message);
        this.webSocketService.sendMessage(message);
        this.chatForm.controls.inputBox.patchValue('');
      }

      // this.webSocketService.sendMessage(message);
    }
  }


  public getMembers(): void {
    this.dataService.getMember().subscribe(
      (response: Member[]) => {
        this.members = [];
        for (let member of response){
          if (member.username !== this.loggedInUsername){
            this.members.push(member);
          }
        }
        this.route.params.subscribe(params => {
          const memberId = +params.memberId;
          if(memberId !== null && memberId !== undefined){
            for(let member of this.members){
              if(member.id === memberId){
                this.selectedMember = member;
                this.getChatMessagesWithSeller(this.selectedMember.username);
              }
            }
          }
        });
      },
      (error: HttpErrorResponse) => {
        this.members =[];
        alert(error.message);
      }
    );
  }

  ngOnInit(): void {
    this.getMembers();
  }


  ngOnDestroy(): void {
    this.notificationSub.unsubscribe();
  }


// public notificationCount = 0;
  //  constructor(private webSocketService: WebSocketServiceService) {
  //    let stompClient = this.webSocketService.connect();
  //    stompClient.connect({}, frames =>{
  //      stompClient.subscribe('/topic/chat',notifications =>{
  //        this.notificationCount = JSON.parse(notifications.body).count;
  //      })
  //    })
  //  }

  // ngOnInit(): void {
  // }
  // public notifications: string[] = [];
  //
  // private client: StompJs.Client;
  // private clientel: SockJS;
  // clientUser;

  // Working connection
  // connectClicked() {
  //   if (!this.client || this.client.connected) {
  //     this.client = new StompJs.Client;
  //     this.client.configure({
  //       webSocketFactory: () => new SockJS('http://localhost:8080/ws'),
  //       debug: (msg: string) => console.log(msg)
  //     });
  //
  //     this.clientUser = Stomp.over(this.client);
  //     this.client.activate();
  //     this.client.subscribe("/topic/news", notificationResponse=>{
  //
  //       console.log(notificationResponse);
  //     });
  //     // this.watchForNotifications();
  //     console.info('connected!');
  //   }
  // }

//   showMessage(message){
//    let messageElement= document.getElementById("messages")
//     return messageElement.append("<tr><td>" + message + "</td></tr>");
//   }
//
//   sendMessage(){
//   console.log("sending message");
//   this.clientUser.send("/topic/chat",{})
// }



   // showMessage(message: String){
   //  elementAt.("#messages").append("<tr><td>" + message +"</td></tr>")
   //
   //
  // let oldContent = "this is a car.";
  // oldContent.replace(new RegExp('car'), match => {
  //   return `<span class="highlight-text" id="car"> + match +</span>`;
  // });
  // let content = this.sanitizer.bypassSecurityTrustHtml(content)

  // sendChatMessage() {
  //   let from = document.getElementById('from').innerText;
  //  let text = document.getElementById('text').innerText;
  //   this.clientel.SockJS.send("/app/chat", {},
  //         JSON.stringify({'from':from, 'text':text}));
  //   Console.log(from,text);
  //  }

  // private watchForNotifications() {
  //   this.client.subscribe('/topic/news')
  //     .pipe(
  //       map((response) => {
  //         const text: string = JSON.parse(response.body).text;
  //         console.log('Got ' + text);
  //         return text;
  //       }))
  //     .subscribe((notification: string) => this.notifications.push(notification));
  // }
 /* connectClicked() {
    if (!this.client || this.client.connected) {
      this.client = new StompJs.Client({
        webSocketFactory: () => new SockJS('http://localhost:8080/ws'),
        debug: (msg: string) => console.log(msg)
      });

      this.client.onConnect = () => {

        this.client.subscribe('/user/notification/item', (response) => {
          const text: string = JSON.parse(response.body).text;
          console.log('Got ' + text);
          this.notifications.push(text);
        });

        console.info('connected!');
      };

      this.client.onStompError = (frame) => {
        console.error(frame.headers['message']);
        console.error('Details:', frame.body);
      };

      this.client.activate();
    }
  }*/
// Disconnected working
//   disconnectClicked() {
//     if (this.client && this.client.connected) {
//       this.client.deactivate();
//       this.client = null;
//       console.info("disconnected :-/");
//     }
//   }

  // startClicked() {
  //   console.log(this.client);
  //   if (this.client && this.client.connected) {
  //     this.client.publish({destination: '/ws/start'});
  //   }
  // }
  //
  // stopClicked() {
  //   console.log(this.client);
  //   if (this.client && this.client.connected) {
  //
  //     this.client.publish({destination: '/ws/stop'});
  //   }
  // }

  // title = 'grokonez';
  // description = 'Angular-WebSocket Demo';
  //
  // greetings: string[] = [];
  // disabled = true;
  // name: string;
  // private stompClient = null;
  //
  // constructor() { }
  //
  // setConnected(connected: boolean) {
  //   this.disabled = !connected;
  //
  //   if (connected) {
  //     this.greetings = [];
  //   }
  // }

  // connect() {
  //   const socket = new SockJS('http://localhost:8080/app');
  //   this.stompClient = Stomp.over(socket);
  //
  //   const _this = this;
  //   this.stompClient.connect({}, function (frame) {
  //     _this.setConnected(true);
  //     console.log('Connected: ' + frame);
  //
  //     _this.stompClient.subscribe('/topic/news', function (hello) {
  //       _this.showGreeting(JSON.parse(hello.body).greeting);
  //     });
  //   });
  // }

  // disconnect() {
  //   if (this.stompClient != null) {
  //     this.stompClient.disconnect();
  //   }
  //
  //   this.setConnected(false);
  //   console.log('Disconnected!');
  // }
  //
  // sendName() {
  //   this.stompClient.send(
  //     '/gkz/hello',
  //     {},
  //     JSON.stringify({ 'name': this.name })
  //   );
  // }
  //
  // showGreeting(message) {
  //   this.greetings.push(message);
  // }

}


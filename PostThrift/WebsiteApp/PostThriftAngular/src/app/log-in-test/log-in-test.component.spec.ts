import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LogInTestComponent } from './log-in-test.component';

describe('LogInTestComponent', () => {
  let component: LogInTestComponent;
  let fixture: ComponentFixture<LogInTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LogInTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LogInTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

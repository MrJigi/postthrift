import {Component, OnInit} from '@angular/core';
import {Product} from "../product";
import {HttpErrorResponse} from "@angular/common/http";
import {Member} from "../member";
import {DataService} from "../data.service";
import {AbstractControl, FormBuilder, FormGroup, NgForm, ValidationErrors, Validators} from "@angular/forms";
import {AuthentificationService} from "../authentification.service";
import {first} from "rxjs/operators";
import {Router} from "@angular/router";
import {UserValidators} from "../validators/user.validators";

@Component({
  selector: 'app-log-in-test',
  templateUrl: './log-in-test.component.html',
  styleUrls: ['./log-in-test.component.css']
})
export class LogInTestComponent implements OnInit{

  // private service: DataService
  registerForm : FormGroup;

  constructor(private service: AuthentificationService,private router: Router, private formBuilder:FormBuilder, private dataService: DataService) {

  }


  submit(login){
    console.log('Form submitted', login);
  }

  passwordMatchCheck() {

  }

  get registerControls(){
    return this.registerForm.controls;
  }
  ngOnInit() {
    this.registerForm = this.formBuilder.group(
      {
        registrationUsername:['',[
          //Validators
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
          UserValidators.cannotContainSpace,
        ]],
        registrationPassword:['',[
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
          UserValidators.cannotContainSpace
        ]],
        firstName:['',[
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
          UserValidators.cannotContainSpace
        ]],
        lastName:['',[
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
          UserValidators.cannotContainSpace
        ]],
        email:['',[
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
          UserValidators.cannotContainSpace
        ]],
        passwordMatch:['',[
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
          UserValidators.cannotContainSpace
        ]]
      },
      {validators : [UserValidators.passwordMatchCheck]}
    );
  }
  RegisteringUser(){
    console.log(this.registerForm);
    if(this.registerForm.invalid && !this.usernameDoesExist()){
      return console.log('user registration error invalid' +this.registerForm);
    }
    this.service.signUp(
      this.registerControls.firstName.value,
      this.registerControls.lastName.value,
      this.registerControls.registrationUsername.value,
      this.registerControls.registrationPassword.value,
      this.registerControls.email.value).subscribe(
        response => {
          this.service.logIn(this.registerControls.registrationUsername.value,
            this.registerControls.registrationPassword.value).pipe(first()).subscribe({
              next: () => {
                this.handleRedirect();
              },
              error: err => {
                alert("No matches");
              }
            }
          );
        },
      registrationError => {
        alert("registration unsuccesful : "+ registrationError.message);
      }
    );


    // this.service.signUp(registration.controls.firstName.value,registration.controls.lastName.value,
    //   registration.controls.registrationUsername.value,registration.controls.registrationPassword.value,
    //   registration.controls.email.value).subscribe(
    //   response => {
    //     // this.service.logIn(registration.controls.registrationUsername.value)
    //
    //     error: err => {
    //       alert("Registration fail")
    //     }
    //   }
    // )

  }
  usernameDoesExist(): any {
    const usernameValue = this.registerForm.get('registrationUsername').value as string;
    this.dataService.getMemberByUsername(usernameValue).subscribe({ next: value => {
      if (value.username === usernameValue){
        alert("user with this username exists");
        return true;
      }
      else {
        return  false;
      }
      },
    });
      }

  logingUser(login : NgForm) {
  if(login.invalid){
    return ;
  }

  this.service.logIn(login.controls.username.value,login.controls.password.value).pipe(first()).subscribe({
    next: () => {

      this.handleRedirect();
    },
    error : err => {
      alert("No matches");
    }
  });
  }


  handleRedirect(){
    const roles : string[] = this.service.getRoles;
    if(roles != null){
      if(roles.indexOf('MEMBER') > -1){
        this.router.navigate(["/"]);
      }
      else if(roles.indexOf('ADMIN') > -1){
        this.router.navigate(["/GetAllMembers"]);
      }
    }
  }



 /* public onRegistration(Form: any ){
    console.log("new item is being submitted");
    let member: Member;

    member = {

      // firstName: Form.value.;
      // lastName: string;
      // username: string;
      // password: string;
      // email: string;
      // DOB: Date;
      // country: string;
      // city: string;
      // profileUrl: string;
      // lastLoginDate: string;
      //
      // joinDate: Date;
      //
      // roles: string;
      // isActive: boolean;



      // name: Form.value.name,
      // price: Form.value.price,
      // description: Form.value.description,
      // city: Form.value.city,
      // category: this.selectedCategory,

    } as Member;

    console.log(member);
    this.service.addMember(member).subscribe(
      (response  ) => {
        alert("new user has been added");

      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }


    );

  }*/

  // constructor() { }

  // ngOnInit(): void {
  // }

}

import { Component, OnInit } from '@angular/core';
import {Product} from "../product";
import {HttpErrorResponse} from "@angular/common/http";
import {DataService} from "../data.service";
import {AuthentificationService} from "../authentification.service";
import {Member} from "../member";

@Component({
  selector: 'app-user-profile-listings',
  templateUrl: './user-profile-listings.component.html',
  styleUrls: ['./user-profile-listings.component.css']
})
export class UserProfileListingsComponent implements OnInit {


  public products: Product[];
  public member: Member;

  // tslint:disable-next-line:no-shadowed-variable
  constructor(private dataService: DataService, private authService: AuthentificationService){}

// tslint:disable-next-line:typedef
  ngOnInit(){
    this.getMemberById();
    this.getProducts();


  }

  public getProducts(): void {
    this.dataService.getProducts().subscribe(
      (response: Product[]) => {
        this.products = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public getMemberById() : void {
    const memberId = this.authService.getUser.id;
    this.dataService.getMemberById(memberId).subscribe(
      (response: any) => {
        this.member = response;
      },
      (error: HttpErrorResponse) => {
        alert("Couldn't get the member error :" + error.message);
      }
    )
  }
}

export class NotificationChat{
  public constructor(public text: string,
                     public sender: string,
                     public time: string,
                     public messageId: number){}

}

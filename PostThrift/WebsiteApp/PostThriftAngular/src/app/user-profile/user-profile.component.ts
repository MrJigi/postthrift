import { Component, OnInit } from '@angular/core';
import {Member} from "../member";
import {DataService} from "../data.service";
import {HttpErrorResponse} from "@angular/common/http";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthentificationService} from "../authentification.service";
import {Product} from "../product";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {

  public member: Member;
  userRole;
  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private pageRouting: Router,
    private authService: AuthentificationService
  ) { }

  ngOnInit(): void {
    // const routeParameter = this.route.snapshot.paramMap;
    // const memberUsername = String(routeParameter.get('username'));
    this.getMemberById();
    this.getUserRole();
  }

  // public getMemberByUsername(memberUsername: string): void{
  // this.dataService.getMemberByUsername("username").subscribe(
  //   (response: any) => {
  //     this.member = response;
  //   },
  //   (error: HttpErrorResponse) => {
  //     alert("Couldn't get the member error :" + error.message);
  //   }
  //
  // )
  // }
  public getMemberAsAdmin(): void {
    // const memberId = this.dataService.getMemberById(1);
  }

  public showInputOnEdit(): void {

  }

  public onSubmit(Form: any ) {
    console.log('new item is being submitted');
    let member: Member;
    // member = {
    //   firstName: Form.value.firstName,
    //   lastName: Form.value.lastName,
    //   // username: Form.value.username,
    //   // email: Form.value.email,
    //   dob: Form.value.date,
    //   country: Form.value.country,
    //   city: Form.value.city,
    // } as Member
    // member = {
    //   firstName: Form.value.firstName,
    //   lastName: Form.value.lastName,
    //   username: Form.value.username,
    //   password: Form.value.password,
    //   email: Form.value.email,
    //   dob: Form.value.date,
    //   country: Form.value.country,
    //   city: Form.value.city
    // } as Member;
    // member.id = this.member;

    console.log(member);
    this.dataService.updateMember(member).subscribe(
      (response) => {
        alert('new item has been submitted');
        this.pageRouting.navigate(['/GetAllProducts']);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  public getMemberById(): void {
    const memberId = this.authService.getUser.id;
    this.dataService.getMemberById(memberId).subscribe(
      (response: any) => {
      this.member = response;
      },
    (error: HttpErrorResponse) => {
      alert("Couldn't get the member error :" + error.message);
    }
   );
  }



  public getUserRole() : void {
    const memberRole = this.authService.getRoles;
    this.userRole = memberRole;
  }




}

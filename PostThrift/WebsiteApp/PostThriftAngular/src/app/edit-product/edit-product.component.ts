import { Component, OnInit } from '@angular/core';
import {Product} from '../product';
import {HttpErrorResponse} from '@angular/common/http';
import {DataService} from '../data.service';
import {ActivatedRoute, Router} from "@angular/router";
import {AuthentificationService} from "../authentification.service";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {

  selectedCategory: string = '';
  categoryList: string[] = [] ;
  productId : number;
  user;
  member;

  constructor(
    private service: DataService ,
    private actRoute: ActivatedRoute,
    private route: Router,
    private authentification : AuthentificationService
  ) {
    this.authentification.user.subscribe(response => {this.user = response;});
  }
  ngOnInit(): void {
    this.getCurrrentLoggedMember();
    this.allCategory();
    this.productId = +this.actRoute.snapshot.paramMap.get('productId');

  }
  // tslint:disable-next-line:typedef
  public onCategorySelect(category: string) {
    this.selectedCategory = category;
  }

  // tslint:disable-next-line:typedef
  public allCategory() {
    this.service.getCategories().subscribe(
      response => {
        this.categoryList = response;
        this.selectedCategory = this.categoryList[0];
      }, (error: HttpErrorResponse) => {
        alert(error.message);
      });
  }
  public getCurrrentLoggedMember() {
    const userId :number  = this.authentification.getUser.id;
    this.member = this.service.getMemberById(userId).subscribe(
      (response:any) => {
        this.member = response;
      },
      (error: HttpErrorResponse) => {
        alert("Couldn't get the member error :" + error.message);
      }
    );

  }

  // tslint:disable-next-line:typedef
  public onSubmit(Form: any ) {
    console.log('new item is being submitted');
    let product: Product;
    product = {
      name: Form.value.name,
      price: Form.value.price,
      description: Form.value.description,
      city: Form.value.city,
      category: this.selectedCategory,

    } as Product;
    product.id = this.productId;
    product.seller = this.member.username;
    console.log(product);
    this.service.updateProduct(product).subscribe(
      (response) => {
        alert('new item has been submitted');
        this.route.navigate(['/GetAllProducts']);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }
  /* public updateProductById(productId): void{
   this.dataService.updateProduct(productId).subscribe(
     (response: any) => {
     },
     (error: HttpErrorResponse) => {
       alert('The product not work');
     }
   );
 }*/


}

import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DataService } from './data.service';
import {Member} from './member';
import { Router } from '@angular/router';
import {AuthentificationService} from './authentification.service';
import {WebSocketsService} from './web-sockets.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Angular app';
  public members: Member[];

  constructor (private DataService: DataService, private router: Router, private authService: AuthentificationService,private webSocket:WebSocketsService){

  }

ngOnInit(){
  let jwtUser = this.authService.getUser;
  if(jwtUser != null && jwtUser != undefined){
    if(this.webSocket.connectionStatus == false){
      this.webSocket.onConnect(jwtUser.id);
    }
  }
  //this.getMember();
}

    public getMember(): void {
      this.DataService.getMember().subscribe(
        (response: Member[]) =>{
          this.members = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
    public onButtonClick(): void {
      this.router.navigate(['GetAllMembers']);
    }

    public getAllProducts(): void {
      this.router.navigate(['GetAllProducts']);
    }



}

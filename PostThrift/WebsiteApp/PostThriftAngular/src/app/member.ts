export interface Member{
  id: number;

  firstName: string;
  lastName: string;
  username: string;
  password: string;
  email: string;
  DOB: Date;
  country: string;
  city: string;
  profileUrl: string;
  lastLoginDateDisplay: Date;
  joinDate: Date;
  //arrays of roles
  roles: string;
  isActive: boolean;



}

import { Injectable } from '@angular/core';
import * as SockJS from 'sockjs-client';
import {environment} from "../environments/environment";
import {Stomp} from "@stomp/stompjs";
import {BehaviorSubject, Observable} from "rxjs";
import {Message} from "./message";
import {NotificationChat} from "./notificationChat";
import {DataService} from "./data.service";

@Injectable({
  providedIn: 'root'
})
export class WebSocketsService {

  //public notifications: string[] = [];

  // private client: StompJs.Client;
  // private clientel: SockJS;
  // clientUser;
  private clientStomp = null;
  private baseSocketUrl = environment.apiBaseUrl + '/ws';
  public connectionStatus = false;
  private chatSubRoute = '/topic/notifications';
  private notificationSubject: BehaviorSubject<NotificationChat[]>;
  public notifications: Observable<NotificationChat[]>;
  private userId = null;


  constructor(private dataService: DataService) {
    this.notificationSubject = new BehaviorSubject<NotificationChat[]>([]);
    this.notifications = this.notificationSubject.asObservable();


  }

  public get getNotifications(): NotificationChat[]{
    return this.notificationSubject.value;
  }



  public onConnect(userId: number){
  this.userId = userId;
    const socket = new SockJS(this.baseSocketUrl);
    this.clientStomp = Stomp.over(socket);

    const service = this;

    this.clientStomp.connect({}, (frame) => {
      this.connectionStatus = true;
      console.log("connecting : " + frame.toString());
     const notificationRoute = `/user/${userId}/queue/notifications`;
      service.clientStomp.subscribe(notificationRoute, (notificationData) => {
        // add you method to do what ya want when a message is received on this sub route

        this.onNewNotificationReceived(notificationData);
        // alert(notificationData.body);
        // this.showMessage(notificationData.body);
      });
    });
  }

  onNewNotificationReceived(notification: any): void{
    console.log(notification + "On new notification");
    let convObj = JSON.parse(notification.body); //added .content
    let notificationItem = new NotificationChat(convObj.text, convObj.sender, convObj.time, convObj.messageId);
    let notificationList: NotificationChat[] = this.getNotifications;
    notificationList.push(notificationItem);
    this.notificationSubject.next(notificationList);
    console.log("Notification List: "+ notificationList);
  }

  public disconnect(){
    if (this.clientStomp != null){
      this.connectionStatus = false;
      this.clientStomp.disconnect();
      this.userId= null;
    }
  }


  showMessage(message){
    let messageElement= document.getElementById("response")
    // return messageElement.append("<div class='messages'><td>" + message + "</td></div>");
    return messageElement.append("<tr><td>" + message + "</td></tr>");
  }



  sendMessage(message: Message){
    const endpoint = '/app/chat';
    if (message.content.trim() !== ''){
      this.clientStomp.send(endpoint, {}, JSON.stringify(message));
    }
  }
  // disconnectClicked() {
  //   if (this.client && this.client.connected) {
  //     this.client.deactivate();
  //     this.client = null;
  //     console.info("disconnected :-/");
  //   }
  // }


}

import { Injectable } from '@angular/core';
import {HttpClient, HttpHandler, HttpHeaders} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../environments/environment';
import { Member } from './member';
import { Product } from './product';
import {AdvertiseRequest} from "./advertiseRequest";
import {Message} from "./message";


@Injectable({
  providedIn: 'root'
})
export class DataService {

  private apiServerUrl = environment.apiBaseUrl;

  // Test for list to show up on the website
  //button/array display
  // info1: string[]= ["John Matt",'E352','email@gmail.com']
  // info2: string[]=["Rose Neiland",'e420','SaintOlaf@gmail.com']
  // info3: string[]=["Blanch Devero",'unknown','southernBell@gmail.com']

  // getInfo1():string[]{
  //   return this.info1
  // }
  // getInfo2():string[]{
  //   return this.info2
  // }
  // getInfo3():string[]{
  //   return this.info3
  // }
  // addInfo(info){
  //   this.info1.push(info)
  //   this.info2.push(info)
  //   this.info3.push(info)
  //   return this.info1
  // }
  constructor(private http: HttpClient) { }



  public generateToken(request){
    return this.http.post(`${this.apiServerUrl}/authentificate`,request,{responseType:'text' as 'json'});
  }
  // public logInUser(request){
  //   //const headers = new HttpHandler({ Authorization: 'Bearer '+ btoa()})
  //   return this.http.post(`${this.apiServerUrl}auth/signin`,{ username: member.username,
  //     password: member.password} );
  //   }

  // public getMemberByUsername(username:string): Observable<Member>{
  //   return this.http.get<Member>(`${this.apiServerUrl}/api/member/getByUsername/`+username);
  // }
  public getMemberByUsername(username: string): Observable<Member>{
    const endPoint = `${this.apiServerUrl}/api/member/getByUsername/` ;
    // let requestObject :
    return this.http.get<Member>(`${endPoint}${username}`);
  }
  public getMemberById(id : number): Observable<Member>{
    const endPoint = `${this.apiServerUrl}/api/member/getById/` ;
    return this.http.get<Member>(`${endPoint}${id}`);
  }

  public getMemberByIdAsAdmin(id : number): Observable<Member>{
    const endPoint = `${this.apiServerUrl}/api/member/getUserAsAdmin/` ;
    return this.http.get<Member>(`${endPoint}${id}`);
  }

  public getMember(): Observable<Member[]>{
    return this.http.get<Member[]>(`${this.apiServerUrl}/api/member/getAllMembers`);
  }


  public addMember(member: Member): Observable<Member>{
    return this.http.post<Member>(`${this.apiServerUrl}/api/member/add`, member);
  }

  public updateMember(member: Member): Observable<Member>{
    return this.http.put<Member>(`${this.apiServerUrl}/api/member/update`, member);
  }

  public deleteMember(memberId: number): Observable<void>{
    return this.http.delete<void>(`${this.apiServerUrl}/api/member/delete/${memberId}`);
  }
  public createProduct(product: AdvertiseRequest): Observable<Product>{
    return this.http.post<Product>(`${this.apiServerUrl}/api/product`, product);
  }
  public getProductByName(productSearched: string): Observable<Product[]>{
    return this.http.get<Product[]>(`${this.apiServerUrl}/api/product/`+ productSearched);
  }
  public getProducts(): Observable<Product[]>{
    return this.http.get<Product[]>(`${this.apiServerUrl}/api/product/getAll`);
  }
  public getProductById(productId: number): Observable<Product>{
    return this.http.get<Product>(`${this.apiServerUrl}/api/product/GetId/${productId}`);
  }
  public deleteProduct(productId: number): Observable<Product>{
    return this.http.delete<Product>(`${this.apiServerUrl}/api/product/removeProduct/${productId}`);
  }
  public updateProduct(product: Product): Observable<Product>{
    return this.http.put<Product>(`${this.apiServerUrl}/api/product/updateProduct/${product.id}`, product);
  }
  public getCategories(): Observable<any>{
    return this.http.get<any>(`${this.apiServerUrl}/api/product/getAllCategory`);
  }
  public getMessageById(messageId: number): Observable<Message>{
    return this.http.get<Message>(`${this.apiServerUrl}/api/messages/${messageId}`);
  }
  public getAllMessages(senderId: number,receiverName: string): Observable<Message[]>{
    return this.http.get<Message[]>(`${this.apiServerUrl}/api/messages/${senderId}/${receiverName}`);
  }
  // public getAllMembers(): Observable<Member[]>{
  //   return this.http.get<Member[]>(`${this.apiServerUrl}/api/statistics/`);
  // }
  // public addMember(employee: Member): Observable<Member>{
  //   return this.http.post<Employee>(`${this.apiServerUrl}/api/member/addMember`. member)
  // }
}

import { Component, OnInit } from '@angular/core';
import {Member} from "../member";
import {DataService} from "../data.service";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpErrorResponse} from "@angular/common/http";
import {AuthentificationService} from "../authentification.service";

@Component({
  selector: 'app-admin-profile-edit',
  templateUrl: './admin-profile-edit.component.html',
  styleUrls: ['./admin-profile-edit.component.css']
})
export class AdminProfileEditComponent implements OnInit {


  // title = 'Angular app';
  // public members: Member[];
  //
  // constructor(private dataService: DataService,private router: Router){}
  //
  // ngOnInit(){
  //   this.getMember();
  // }
  //
  // public getMember(): void {
  //   this.dataService.getMember().subscribe(
  //     (response: Member[]) => {
  //       this.members = response;
  //     },
  //     (error: HttpErrorResponse) => {
  //       alert(error.message);
  //     }
  //   );
  // }
  //
  public member: Member;
  userRole;
  memberIdFromLink;

  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private pageRouting: Router,
    private authService: AuthentificationService
  ) { }


  ngOnInit(): void {
    const routeParameters = this.route.snapshot.paramMap;
    this.memberIdFromLink = Number(routeParameters.get('memberId'));
    // const routeParameter = this.route.snapshot.paramMap;
    // const memberUsername = String(routeParameter.get('username'));
    this.getMemberById(this.memberIdFromLink);
    this.getUserRole();

  }

  // public getMemberByUsername(memberUsername: string): void{
  // this.dataService.getMemberByUsername("username").subscribe(
  //   (response: any) => {
  //     this.member = response;
  //   },
  //   (error: HttpErrorResponse) => {
  //     alert("Couldn't get the member error :" + error.message);
  //   }
  //
  // )
  // }
  public getMemberAsAdmin(): void {
    // const memberId = this.dataService.getMemberById(1);
  }

  public getMemberById(idNumber: number): void {

    this.dataService.getMemberById(idNumber).subscribe(
      (response: any) => {
        this.member = response;
      },
      (error: HttpErrorResponse) => {
        alert("Couldn't get the member error :" + error.message);
      }
    );
  }

  public getUserRole() : void {
    const memberRole = this.authService.getRoles;
    this.userRole = memberRole;
  }




}

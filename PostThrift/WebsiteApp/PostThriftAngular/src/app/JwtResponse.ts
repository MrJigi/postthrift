export class JwtResponse{
  public constructor(public token: string, public type: string, public id: number, public roles: string[]) {

  }

}

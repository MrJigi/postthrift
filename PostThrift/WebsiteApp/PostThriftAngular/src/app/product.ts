export interface Product{
    id: number;
    name: string;
    seller: string;
    price: string;
    description: string;
    creationDate: string;
    city: string;
    category: string;

}

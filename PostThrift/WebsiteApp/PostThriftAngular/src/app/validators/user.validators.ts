import {AbstractControl, ValidationErrors} from "@angular/forms";
import {DataService} from "../data.service";
import {HttpErrorResponse} from "@angular/common/http";

export class UserValidators{

  constructor(dataService: DataService) {
  }
  static cannotContainSpace(control : AbstractControl) : ValidationErrors | null {
    if((control.value as string).indexOf(' ')>= 0){
      return {cannotContainSpace : true};
    }
    else {
      return null;
    }

  }

  static passwordMatchCheck(registerControl: AbstractControl): ValidationErrors | null {
    const passwordValue = registerControl.get('registrationPassword').value as string;
    const confirmPasswordValue = registerControl.get('passwordMatch').value as string;
    if (passwordValue !== confirmPasswordValue){
      return {passwordNotMatching : true};
    }
    else{
      return null;
    }
  }

  usernameDoesExist(registerControl: AbstractControl, usernameCheck: string): ValidationErrors | null {
    const usernameValue = registerControl.get('registrationUsername').value as string;
        if (usernameValue === usernameCheck){
          return{usernameDoesExist : true} ;
        }
        else {
          return{usernameDoesExist : false} ;
        }
      }
  // public getMemberById(username: string): void {
  //
  //   this.dataService.getMemberByUsername(username).subscribe(
  //     (response: any) => {
  //       this.member = response;
  //     },
  //     (error: HttpErrorResponse) => {
  //       alert("Couldn't get the member error :" + error.message);
  //     }
  //   );
  // }


}

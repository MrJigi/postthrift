import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { DataService } from './../data.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-product-cards',
  templateUrl: './product-cards.component.html',
  styleUrls: ['./product-cards.component.css']
})
export class ProductCardsComponent implements OnInit {


  public products: Product[];

  // tslint:disable-next-line:no-shadowed-variable
  constructor(private DataService: DataService){}

// tslint:disable-next-line:typedef
ngOnInit(){
  this.getProducts();

}

    public getProducts(): void {
      this.DataService.getProducts().subscribe(
        (response: Product[]) => {
          this.products = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
}

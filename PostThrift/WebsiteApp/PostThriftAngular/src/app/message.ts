export class Message{
  public constructor(public messageId: number,
                     public chatId: string,
                     public senderId: number,
                     public senderName: string,
                     public receiverId: number,
                     public receiverName: string,
                     public content: string,
                     public messageStatus: string,
                     public messageTimeStamp: Date){

  }
}

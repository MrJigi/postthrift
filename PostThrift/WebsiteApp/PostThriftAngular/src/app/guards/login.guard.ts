import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,

} from "@angular/router";
import {AuthentificationService} from "../authentification.service";

@Injectable()
export class LoginGuard implements CanActivate{
  constructor(private router: Router, private jwtAuth: AuthentificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot ){
    const user = this.jwtAuth.getUser;
    if(user == null){
      return true;
    }
    else{
      this.router.navigate(["/"], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }
}

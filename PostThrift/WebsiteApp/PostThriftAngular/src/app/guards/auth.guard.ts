import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,

} from "@angular/router";
import {AuthentificationService} from "../authentification.service";

@Injectable()
export class AuthGuard implements CanActivate{
  constructor(private router: Router, private jwtAuth: AuthentificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot ){
    if(this.jwtAuth.getUser){
      return true;
    }
    else{
      this.router.navigate(["/LogInComponent"], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }
}

import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  Router,

} from "@angular/router";
import {AuthentificationService} from "../authentification.service";

@Injectable()
export class AdminGuard implements CanActivate{
  constructor(private router: Router, private jwtAuth: AuthentificationService) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot ){
    if(this.jwtAuth.getUser != null){
      const index = this.jwtAuth.getRoles.indexOf('ADMIN');
      if(index > -1){
        return true;
      }
      else {
        this.router.navigate(["/"], {
        queryParams: {
          return: state.url
        }
      });
        return false;

      }
    }
    else{
      this.router.navigate(["/LogInComponent"], {
        queryParams: {
          return: state.url
        }
      });
      return false;
    }
  }
}

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {AuthentificationService} from "../authentification.service";

@Component({
  selector: 'app-component-nav-bar',
  templateUrl: './component-nav-bar.component.html',
  styleUrls: ['./component-nav-bar.component.css']
})
export class ComponentNavBarComponent  {

  user;
  role;

  constructor (private router: Router, private authentification : AuthentificationService){
    this.authentification.user.subscribe(response => {this.user = response;});
  }

  public logOut(): void{
    this.authentification.logOut();
  }
  checkUserUsername() : any {
    if(this.user != null){
      const username : string = this.user.username;
    return username;
    }
    else{
      return null;
    }
  }
  checkRole(userRole : string): boolean{
    if (this.user != null){
      const roles: string[] = this.authentification.getRoles;
      for (const role of roles){
        if (role === userRole){
          this.role = userRole;
          return true;
        }

      }
      return false;
    }
    else{
      return false;
    }
  }

  rerauteOnUser(): boolean {
    if(this.user === null || this.user === undefined){
      this.navigateToLogIn();
      return false;
    }
    else{
      this.router.navigate(['CreateProduct']);
      return true;
    }
  }

  public onButtonClick(): void {
    this.router.navigate(['GetAllProducts']);
  }
  public navigateToLogIn(): void{
    this.router.navigate(['LogInComponent']);
  }


}

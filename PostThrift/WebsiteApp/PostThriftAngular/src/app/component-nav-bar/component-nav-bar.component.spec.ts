import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentNavBarComponent } from './component-nav-bar.component';

describe('ComponentNavBarComponent', () => {
  let component: ComponentNavBarComponent;
  let fixture: ComponentFixture<ComponentNavBarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentNavBarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

export class HttpResponse {
  public constructor(
    public httpStatusCode: number,
  public httpStatus : string,
  public reason : string,
  public message: string
  ) {

  }


}

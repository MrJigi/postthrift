import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {AuthentificationService} from "../authentification.service";

@Component({
  selector: 'app-admin-panel',
  templateUrl: './admin-panel.component.html',
  styleUrls: ['./admin-panel.component.css']
})
export class AdminPanelComponent implements OnInit {

  user;

  constructor (private router: Router, private authentification : AuthentificationService){
    this.authentification.user.subscribe(response => {this.user = response;});
  }

  ngOnInit(): void {
  }

  checkRole(userRole : string): boolean{
    if (this.user != null){
      const roles: string[] = this.authentification.getRoles;
      for (const role of roles){
        if (role === userRole){
          return true;
        }

      }
      return false;
    }
    else{
      return false;
    }
  }
}

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponentMemberCardsComponent } from './component-member-cards/component-member-cards.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProductCardsComponent } from './product-cards/product-cards.component';
import { HomeComponent } from './home/home.component';
import { UserInfoComponent } from './user-info/user-info.component';
import { LogInTestComponent } from './log-in-test/log-in-test.component';
import {ProductDisplayComponent} from './product-display/product-display.component';
import {CreateListingComponent} from './create-listing/create-listing.component';
import {EditProductComponent} from "./edit-product/edit-product.component";
import {LoginGuard} from "./guards/login.guard";
import {AdminGuard} from "./guards/admin.guard";
import {UserProfileComponent} from "./user-profile/user-profile.component";
import {UserProfileListingsComponent} from "./user-profile-listings/user-profile-listings.component";
import {ComponentNavBarComponent} from "./component-nav-bar/component-nav-bar.component";
import {AdminPanelComponent} from "./admin-panel/admin-panel.component";
import {NotificationSystemComponent} from "./notification-system/notification-system.component";
import {AdminProfileEditComponent} from "./admin-profile-edit/admin-profile-edit.component";

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'GetAllMembers', component: ComponentMemberCardsComponent, canActivate: [AdminGuard] },
  {path: 'GetUserProfile/:username', component: UserProfileComponent},
  {path: 'GetAdminPanel', component: AdminPanelComponent},
  {path: 'GetUserAsAdmin/:memberId', component: AdminProfileEditComponent, canActivate:[AdminGuard] },
  {path: 'GetUserListedItems', component:UserProfileListingsComponent},
  {path: 'GetAllProducts', component: ProductCardsComponent},
  {path: 'GetContactInfo', component: UserInfoComponent},
  {path: 'LogInComponent', component: LogInTestComponent, canActivate: [LoginGuard]},
  {path: 'GetProductDisplay/:productId', component: ProductDisplayComponent},
  {path: 'CreateProduct', component: CreateListingComponent},
   {path: 'MessageTheUser', component: NotificationSystemComponent},
  {path: 'MessageTheUser/:productId', component: NotificationSystemComponent},
  {path: 'updateProduct/:productId', component: EditProductComponent },
  {path: 'NotFound', component: NotFoundComponent },
  {path: '**', redirectTo: '/NotFound'}


];

@NgModule({
  imports: [RouterModule.forRoot(routes,{useHash: true})],





exports: [RouterModule]
})
export class AppRoutingModule { }


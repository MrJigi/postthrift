import { Component, OnInit } from '@angular/core';
import { Member } from './../member';
import { DataService } from './../data.service';
import { HttpErrorResponse } from '@angular/common/http';
import {Router} from "@angular/router";

@Component({
  selector: 'app-component-member-cards',
  templateUrl: './component-member-cards.component.html',
  styleUrls: ['./component-member-cards.component.css']
})
export class ComponentMemberCardsComponent implements OnInit {

  title = 'Angular app';
  public members: Member[];

  constructor(private dataService: DataService,private router: Router){}

ngOnInit(){
  this.getMember();
}

    public getMember(): void {
      this.dataService.getMember().subscribe(
        (response: Member[]) => {
          this.members = response;
        },
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
    }
}

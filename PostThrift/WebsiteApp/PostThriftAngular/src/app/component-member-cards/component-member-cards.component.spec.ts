import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentMemberCardsComponent } from './component-member-cards.component';

describe('ComponentMemberCardsComponent', () => {
  let component: ComponentMemberCardsComponent;
  let fixture: ComponentFixture<ComponentMemberCardsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentMemberCardsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentMemberCardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

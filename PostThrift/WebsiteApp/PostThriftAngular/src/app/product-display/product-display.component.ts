import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from '../product';
import {HttpErrorResponse} from '@angular/common/http';
import {DataService} from '../data.service';
import {AuthentificationService} from "../authentification.service";
import {Member} from "../member";

@Component({
  selector: 'app-product-display',
  templateUrl: './product-display.component.html',
  styleUrls: ['./product-display.component.css']
})
export class ProductDisplayComponent implements OnInit {

  public product: Product;
  member;
  loggedInUsername = '';


  ngOnInit(): void {
    const routeParameters = this.route.snapshot.paramMap;
    const productIdFromRoute = Number(routeParameters.get('productId'));
    this.getProductById(productIdFromRoute);
    this.getMemberLoggedIn();
    console.log(this.member);
    /*this.product = (product => product.id === productIdFromRoute);*/
  }
  constructor(
    private route: ActivatedRoute,
    private dataService: DataService,
    private pageRouting: Router,
    private authService: AuthentificationService
  ) {
    if(this.authService.getUser !== null && this.authService.getUser !== undefined){
      this.dataService.getMemberById(this.authService.getUser.id).subscribe(
        data => {
          this.loggedInUsername = data.username;
        }
      );
    }
  }

  public getMemberLoggedIn() {
    console.log('Getting logged usser');
    let currentUserId = this.authService.getUser.id;
    this.member =  this.dataService.getMemberById(currentUserId).subscribe(
      (response:Member) =>{
        this.member = response;
      },
      (error: HttpErrorResponse) => {
        alert("could not get member")
      }
        // this.pageRouting.navigate(['**']);
    );
  }

  public getProductById(productId): void{
    this.dataService.getProductById(productId).subscribe(
      (response: Product) => {
        this.product = response;
        console.log(this.product);
        },
      (error: HttpErrorResponse) => {
        this.pageRouting.navigate(['**']);
        //alert(error.message);
      }
    );
  }

  public deleteProductById(productId): void{
    this.dataService.deleteProduct(productId).subscribe(
      (response: any) => {
        alert('The product was deleted');
        this.pageRouting.navigate(['/GetAllProducts']);
      },
      (error: HttpErrorResponse) => {
        alert('The product not work');
      }
    );
  }

  public onUpdateClick(): void{
    this.pageRouting.navigate(['/updateProduct']);
  }

  public updateProductById(productId): void{
    this.dataService.updateProduct(productId).subscribe(
      (response: any) => {
      },
      (error: HttpErrorResponse) => {
        alert('The product is not defined');
      }
    );
  }

  public checkIfMemberCanChat(product: Product): boolean{
    if (this.loggedInUsername !== ''){
      if (this.loggedInUsername !== product.seller){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }

  public chatWithSeller(memberId: number): void{
    const url = `/MessageTheUser/${memberId}`;
    this.pageRouting.navigate([url]);
  }

/*  public onAddEmployee(addForm: NgForm): void{
    this.dataService.addMember()
  }*/


/*public getProducts(): void {
  this.DataService.getProductsById().subscribe(
    (response: Product[]) => {
      this.products = response;
    },
    (error: HttpErrorResponse) => {
      alert(error.message);
    }
  );*/

  }

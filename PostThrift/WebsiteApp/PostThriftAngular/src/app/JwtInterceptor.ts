import {Injectable} from "@angular/core";
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";
import {AuthentificationService} from "./authentification.service";

@Injectable()
export class JwtInterceptor implements HttpInterceptor{
  // initialise the authenticationService
  constructor(private authenticationService: AuthentificationService) {
  }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const user = this.authenticationService.getUser;
    const isLoggedIn = user && user.token;
    if (isLoggedIn){
      req = req.clone({
        setHeaders: {
          Authorization: `${user.type} ${user.token}`
        }
      });
    }
    return next.handle(req);
  }
}

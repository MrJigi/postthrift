import {Product} from "./product";

export interface AdvertiseRequest{
  sellerUsername: String;
  product : Product;

}

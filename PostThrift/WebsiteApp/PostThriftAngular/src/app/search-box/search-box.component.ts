import { Component, OnInit } from '@angular/core';
import {DataService} from "../data.service";
import {ActivatedRoute, Data, Router} from "@angular/router";
import {Product} from "../product";
import {HttpErrorResponse} from "@angular/common/http";
import {AuthentificationService} from "../authentification.service";
import {FormBuilder} from "@angular/forms";

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.css']
})
export class SearchBoxComponent implements OnInit {

  searchedItems: any;

  constructor( private dataService: DataService,
               private formBuilder:FormBuilder
               ) { }

  ngOnInit(): void {
  }


  // public getProductById(productId): void{
  //   this.dataService.getProductById(productId).subscribe(
  //     (response: Product) => {
  //       this.product = response; },
  //     (error: HttpErrorResponse) => {
  //       this.pageRouting.navigate(['**']);
  //       //alert(error.message);
  //     }
  //   );
  // }

  // getProductByName() {
  //   return this.dataService.getProductByName(search.).subscribe(
  //     (response) => {
  //       this.searchedItems = response;},
  //     (error: HttpErrorResponse) => {
  //       alert("no items were found");
  //   }
  //   );
  // }
}

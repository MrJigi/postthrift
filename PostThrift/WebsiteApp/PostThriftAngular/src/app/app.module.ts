import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TextcomponentComponent } from './textcomponent/textcomponent.component';

import { LogInTestComponent } from './log-in-test/log-in-test.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserInfoComponent } from './user-info/user-info.component';
import { ComponentNavBarComponent } from './component-nav-bar/component-nav-bar.component';
import { ComponentHeaderComponent } from './component-header/component-header.component';
import { ComponentPageFeaturesComponent } from './component-page-features/component-page-features.component';
import { ComponentFooterComponent } from './component-footer/component-footer.component';
import { DataService } from './data.service';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import { ComponentMemberCardsComponent } from './component-member-cards/component-member-cards.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ProductCardsComponent } from './product-cards/product-cards.component';
import { HomeComponent } from './home/home.component';
import { CreateListingComponent } from './create-listing/create-listing.component';
import { ProductDisplayComponent } from './product-display/product-display.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { EditProductComponent } from './edit-product/edit-product.component';
import {AuthentificationService} from "./authentification.service";
import {TokenStorageService} from "./token-storage.service";
import {JwtInterceptor} from "./JwtInterceptor";
import {ErrorInterceptor} from "./ErrorInterceptor";
import {LoginGuard} from "./guards/login.guard";
import {AuthGuard} from "./guards/auth.guard";
import {AdminGuard} from "./guards/admin.guard";
import { UserProfileComponent } from './user-profile/user-profile.component';
import { UserProfileListingsComponent } from './user-profile-listings/user-profile-listings.component';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { NotificationSystemComponent } from './notification-system/notification-system.component';
import { AdminProfileEditComponent } from './admin-profile-edit/admin-profile-edit.component';
import { StatisticsComponent } from './statistics/statistics.component';


@NgModule({
  declarations: [
    AppComponent,
    TextcomponentComponent,
    LogInTestComponent,
    UserInfoComponent,
    ComponentNavBarComponent,
    ComponentHeaderComponent,
    ComponentPageFeaturesComponent,
    ComponentFooterComponent,
    ComponentMemberCardsComponent,
    NotFoundComponent,
    ProductCardsComponent,
    HomeComponent,
    CreateListingComponent,
    ProductDisplayComponent,
    SearchBoxComponent,
    EditProductComponent,
    UserProfileComponent,
    UserProfileListingsComponent,
    AdminPanelComponent,
    NotificationSystemComponent,
    AdminProfileEditComponent,
    StatisticsComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    AuthentificationService,
    TokenStorageService,
    LoginGuard,
    AuthGuard,
    AdminGuard,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

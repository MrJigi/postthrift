import { Injectable } from '@angular/core';
import {environment} from "../environments/environment";
import {BehaviorSubject, Observable} from "rxjs";
import {JwtResponse} from "./JwtResponse";
import {Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {TokenStorageService} from "./token-storage.service";
import {SignInRequest} from "./SignInRequest";
import {map} from "rxjs/operators";
import {SignUpRequest} from "./SignUpRequest";
import {Member} from "./member";
import {HttpResponse} from "./httpResponse";
import {WebSocketsService} from "./web-sockets.service";

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  private BaseUrl = environment.apiBaseUrl + '/api/auth';
  private userSubject : BehaviorSubject<JwtResponse>;
  public user : Observable<JwtResponse>;

  constructor(private websocket:WebSocketsService, private router : Router, private http : HttpClient,private tokenStorage : TokenStorageService) {
    this.userSubject = new BehaviorSubject<JwtResponse>(null);
    this.user = this.userSubject.asObservable();
    if (this.tokenStorage.getUser()!= null){
      this.userSubject.next(this.tokenStorage.getUser())
    }
  }
  public get getUser() : JwtResponse{
    return this.userSubject.value;
  }
  public get getRoles() : string[] | null{
    if(this.getUser != null){
      return this.getUser.roles;
    }
    else{
      return null;
    }
  }
public logOut(): void{
    this.tokenStorage.clearUser();
    this.userSubject.next(null);
    this.websocket.disconnect();
    this.router.navigate(["/LogInComponent"]);
}

  public logIn(Username: string , Password: string) : Observable<JwtResponse>{
    const endPoint = `${this.BaseUrl}/signin`;
    const requestObject : SignInRequest = new SignInRequest(Username,Password);

    return this.http.post<any>(endPoint,requestObject,{withCredentials:true}).pipe(map(response => {
      const jwtResponse : JwtResponse = new JwtResponse(response.token,response.type,response.id,response.roles);
      this.tokenStorage.saveUser(jwtResponse);
      this.userSubject.next(jwtResponse);
      this.websocket.onConnect(jwtResponse.id);
      return jwtResponse;
    }));
  }

  /*public addMember(member: Member): Observable<Member>{
    return this.http.post<Member>(`${this.apiServerUrl}/api/member/add`, member);
  }*/
  public signUp(FirstName:string, LastName:string ,Username: string, Password: string, Email: string): Observable<HttpResponse>{
    const endPoint = `${this.BaseUrl}/signup`;
    let requestObject : SignUpRequest = new SignUpRequest(FirstName,LastName,Username,Password,Email);

    return this.http.post<HttpResponse>(endPoint,requestObject);

  }

  public getMemberByUsername(username: String): Observable<Member>{
    const endPoint = `${this.BaseUrl}/getByUsername/` ;
    // let requestObject :
    return this.http.get<Member>(`${endPoint}+${username}`);
  }



}

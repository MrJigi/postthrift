package com.app.postThrift.Websockets.Message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectedEvent;

@Component
public class WebSocketEventListener
{
    private static final Logger LOGGER = LoggerFactory.getLogger(WebSocketEventListener.class);

    @Autowired
    private SimpMessageSendingOperations sendingOperations;

    @EventListener
    public void handleWebSocketConnectListener(final SessionConnectedEvent event){
        LOGGER.info("Someone connected to the chat");
    }
//    @EventListener
//    public void handleWebSocketDisconnectListener(final SessionDisconnectEvent event){
//        final StompHeaderAccessor headerAcessor = StompHeaderAccessor.wrap(event.getMessage());
//
//        final String username = (String) headerAcessor.getSessionAttributes().get("username");
//
////        final Notification notification = Notification.builder()
////                .type(MessageType.DISCONNECT)
////                .sender(username)
////                .build();
//
//        sendingOperations.convertAndSend("/topic/public");
//    }


}

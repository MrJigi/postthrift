package com.app.postThrift.models;

public class AdvertiseRequest {
    private String sellerUsername;
    private Product product;

    public AdvertiseRequest(String sellerUsername, Product product) {
        this.sellerUsername = sellerUsername;
        this.product = product;
    }

    public AdvertiseRequest() {
    }

    public String getSellerUsername() {
        return sellerUsername;
    }

    public void setSellerUsername(String sellerUsername) {
        this.sellerUsername = sellerUsername;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}

package com.app.postThrift.Websockets.Message;

//import lombok.Builder;
//import lombok.Getter;
//import lombok.Setter;
//
//@Builder
public class Notification {


//    @Getter
//    @Setter
    public String text;
//    @Getter
//    @Setter
    private String sender;
//    @Getter
//    @Setter
    private String time;
//    @Getter
//    @Setter
    private Integer messageId;


;

    public Notification(String text, String sender, String time, Integer messageId) {
        this.text = text;
        this.sender = sender;
        this.time = time;
        this.messageId = messageId;
    }

    public Integer getMessageId() {
        return messageId;
    }

    public void setMessageId(Integer messageId) {
        this.messageId = messageId;
    }

    public Notification() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }


}

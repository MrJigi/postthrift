package com.app.postThrift.jwt;

import com.google.common.net.HttpHeaders;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


//@EnableConfigurationProperties
//@ConfigurationProperties(prefix = "application.jwt" )
@Component
public class JwtConfig {

    @Value("${postThrift.jwt.secretKey}")
    private String secretKey;
    @Value("${postThrift.jwt.tokenPrefix}")
    private String tokenPrefix;
    @Value("${postThrift.jwt.tokenExpirationAfterDays}")
    private Integer tokenExpirationAfterDays;


    public JwtConfig() {
    }


    public JwtConfig(String secretKey, String tokenPrefix, Integer tokenExpirationAfterDays) {
        this.secretKey = secretKey;
        this.tokenPrefix = tokenPrefix;
        this.tokenExpirationAfterDays = tokenExpirationAfterDays;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getTokenPrefix() {
        return tokenPrefix;
    }

    public void setTokenPrefix(String tokenPrefix) {
        this.tokenPrefix = tokenPrefix;
    }

    public Integer getTokenExpirationAfterDays() {
        return tokenExpirationAfterDays;
    }

    public void setTokenExpirationAfterDays(Integer tokenExpirationAfterDays) {
        this.tokenExpirationAfterDays = tokenExpirationAfterDays;
    }
    public String getAuthorizationHeader() { return HttpHeaders.AUTHORIZATION;}
}

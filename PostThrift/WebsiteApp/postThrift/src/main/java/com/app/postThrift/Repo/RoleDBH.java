package com.app.postThrift.Repo;

import com.app.postThrift.models.Role;
import com.app.postThrift.models.userRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleDBH extends JpaRepository<Role,Integer> {

    Role findByName(userRoles name);
    Role getById(Integer id);
}

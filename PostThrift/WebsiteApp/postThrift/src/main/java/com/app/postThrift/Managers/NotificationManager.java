package com.app.postThrift.Managers;

import com.app.postThrift.Websockets.Message.Message;
import com.app.postThrift.Websockets.Message.Notification;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
public class NotificationManager {

    private SimpMessagingTemplate template;

    public NotificationManager(SimpMessagingTemplate template) {
        this.template = template;
    }

    @Async("asyncExecutor")
    public void createAndSendNotification(String receiver, Message message) {
        template.convertAndSendToUser(receiver,"/queue/notifications",new Notification(message.getContent(),message.getSenderName(), LocalDate.now().toString(),message.getMessageId()));

    }

}

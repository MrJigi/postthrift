package com.app.postThrift.Websockets.Message;

public enum MessageType {
    CHAT,
    CONNECT,
    DISCONNECT
}

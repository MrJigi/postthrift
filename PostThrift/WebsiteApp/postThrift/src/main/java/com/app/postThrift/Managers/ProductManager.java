package com.app.postThrift.Managers;


import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.exceptions.InvalidDataException;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.CategoryType;
import com.app.postThrift.models.Product;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

@Service
@Validated
public class ProductManager {
    private ProductDBH productDBH;
    private MemberDBH memberDBH;
    private CategoryType CategoryType;

    public ProductManager(ProductDBH productDBH, MemberDBH memberDBH) {
        this.productDBH = productDBH;
        this.memberDBH = memberDBH;
    }

    public List<Product> productList = new ArrayList<Product>();



    public List<CategoryType> getAllCategory(){
    return new ArrayList<>(EnumSet.allOf((com.app.postThrift.models.CategoryType.class)));
    }
    public Product createProduct(@Valid Product product, String username) throws InvalidDataException {
        if(product != null)
        {
            if(memberDBH.getByUsername(username)!= null) {
                product.setSeller(username);
                product.setCreationDate(LocalDate.now());
                productDBH.save(product);
                return product;
            }
            else{
                throw new ResourceNotFoundException("No member found with the given username");
            }
        }
        else{
            throw new InvalidDataException("The given product is invalid"); // replace with another custom exp
        }

    }

    public boolean deleteProduct(Product product){
        if(product!= null){
            productDBH.delete(product);
            return true;
        }
        else{
            return false;
        }
    }

    public Product updateProduct(Product product, int id){
        Product updatableProduct = productDBH.getById(id);
        if(updatableProduct != null){
            product.setId(updatableProduct.getId());
            productDBH.save(product);
            return product;
        }
        return null;
    }


    public Product testIfExists(String searchName) {
        Product foundProduct = productDBH.getByName(searchName);
        if (foundProduct != null) {
            return foundProduct;
        }
        return null;
    }

    public Product getProductById(Integer id){
        Product product1 = productDBH.getById(id);
        if(product1 != null){
            return product1;
        }
        return null;
    }
    public List<Product> getAllProducts(){
        return productDBH.findAll();
    }





}

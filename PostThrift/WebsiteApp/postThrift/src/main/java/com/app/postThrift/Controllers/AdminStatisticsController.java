package com.app.postThrift.Controllers;

import com.app.postThrift.Managers.MemberManager;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.models.userRoles;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="api/statistics")
public class AdminStatisticsController {

    private MemberDBH memberDbh;
    private MemberManager memberManager;

    public AdminStatisticsController(MemberDBH memberDbh, MemberManager memberManager) {
        this.memberDbh = memberDbh;
        this.memberManager = memberManager;
    }

//    @PreAuthorize("hasRole('ADMIN')")
//    @GetMapping(path = "/userRoles")
//    public ResponseEntity<Object> getAllUserRolesAmount(){
//        return ResponseEntity.ok(memberDbh.countAllByActiveEqualsAndRoles(true, userRoles.ROLE_ADMIN));
//    }
}

package com.app.postThrift.Repo;

import com.app.postThrift.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDBH extends JpaRepository<Product, Integer> {

    Product getById(Integer id);
    Product getByName(String name);
    Product getByPriceBefore(String price);

    Product getProductsByCity(String city);
    Product getProductsByPrice(int price);
    Product getProductsByNameContains(String name);



    //void saveAll(List<Product> productList);
}

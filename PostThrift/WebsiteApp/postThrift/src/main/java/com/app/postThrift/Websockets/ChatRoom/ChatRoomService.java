package com.app.postThrift.Websockets.ChatRoom;

import com.app.postThrift.Repo.ChatMessageDBH;
import com.app.postThrift.Repo.ChatRoomDBH;
import org.springframework.stereotype.Service;

@Service
public class ChatRoomService {
    private ChatRoomDBH chatRoomRepo;
    private ChatMessageDBH chatMessageRepo;

    public ChatRoomService(ChatRoomDBH chatRoomRepo, ChatMessageDBH chatMessageRepo) {
        this.chatRoomRepo = chatRoomRepo;
        this.chatMessageRepo = chatMessageRepo;
    }

    public String GetChatId(Integer senderId, Integer receiverId){
        ChatRoom chatRoom = chatRoomRepo.findBySenderIdAndRecieverId(senderId,receiverId);
        if(chatRoom != null){
            return chatRoom.getChatId();
        }
        else{
            return null;

        }
    }

    public void createRoom(String chatId,Integer senderId, Integer receiverId){
        ChatRoom senderChat = new ChatRoom();

//        String chatIdConCat = String.format("%s_%s",senderId,receiverId);
        senderChat.setChatId(chatId);
        senderChat.setSenderId(senderId);
        senderChat.setRecieverId(receiverId);

        ChatRoom receiverChat = new ChatRoom();

        receiverChat.setChatId(chatId);
        receiverChat.setSenderId(receiverId);
        receiverChat.setRecieverId(senderId);

        chatRoomRepo.save(senderChat);
        chatRoomRepo.save(receiverChat);
    }

}

package com.app.postThrift.Controllers;

import com.app.postThrift.Managers.MemberManager;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.Member;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "api/member")
public class MemberController {
    private MemberManager memberManager ;

    @Autowired
    public MemberController(MemberManager memberManager)
    {
        this.memberManager = memberManager;
    }
    //☺
    @RequestMapping(value = "/getById/{id}",method = RequestMethod.GET)
    @PreAuthorize("hasRole('MEMBER')" +
            " || hasRole('ADMIN')")
    public ResponseEntity GetById(@PathVariable("id") int id){
       // memberManager.insertData();
        if(memberManager.getMemberById(id)!= null){
            return ResponseEntity.ok(memberManager.getMemberById(id));
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Member was not found");
        }
    }
    @RequestMapping(value = "/getUserAsAdmin/{id}",method = RequestMethod.GET)
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity GetUserById(@PathVariable("id") int id){
        // memberManager.insertData();
        if(memberManager.getMemberById(id)!= null){
            return ResponseEntity.ok(memberManager.getMemberById(id));
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Member was not found");
        }
    }
    @RequestMapping(value = "getByUsername/{username}")
    @PreAuthorize("hasRole('MEMBER')" +
            " || hasRole('ADMIN')")
    public ResponseEntity GetUserByUsername(@PathVariable("username") String username){
        if(memberManager.getMemberByUsername(username)!= null){
            return ResponseEntity.ok(memberManager.getMemberByUsername(username));
        }
        else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Member was not found");
        }
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("hasRole('MEMBER')")
    public ResponseEntity CreateMember (@RequestBody Member member){
        try{
            Member createdMember = memberManager.createMember(member);
            return new ResponseEntity<>(createdMember,HttpStatus.CREATED);
        }
        catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
//     Set<Role> roles = new HashSet<>();
//        Role roleMember = roleDbh.getById(1);
//        roles.add(roleMember);
//        member.setRoles(roles);

    @RequestMapping(method = RequestMethod.GET , value = "/getAllMembers")
    @PreAuthorize("hasRole('ADMIN') or hasRole('MEMBER')")
    public ResponseEntity getAllMembers(){
        if(memberManager.getAllMembers() != null) {
            return ResponseEntity.ok(memberManager.getAllMembers());
        }
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No members were found");
    }

    @PutMapping(path = "/updateMember/{id}")
    @PreAuthorize("hasRole('MEMBER')" +
            " || hasRole('ADMIN')")
    public ResponseEntity<Member> updateMember(@PathVariable("id") Integer id, @RequestBody Member member) {
        memberManager.updateMember(member,id);
        Member updatableMember =  memberManager.updateMember(member,id);
        if ( updatableMember != null){
            return ResponseEntity.ok(member);
        }
        else{
            throw new ResourceNotFoundException("Update was unsuccessful");
        }
    }




    @DeleteMapping(path = "/removeMember/{id}")
    /*@ResponseStatus(HttpStatus.OK)*/
    @PreAuthorize("hasRole('MEMBER') or hasRole('ADMIN')")
    public void delete(@PathVariable("id") Integer id) {
        Member terminatingMember = memberManager.getMemberById(id) ;
        if (terminatingMember != null) {
            memberManager.deleteMember(terminatingMember.getId());
            ResponseEntity.ok("Terminated" +id+"succesfully");
        }
        else{
            throw new ResourceNotFoundException("Termination was unsuccessful");
        }

    }



}

package com.app.postThrift.models;

public enum CategoryType {
    Classifieds,
    Clothing,
    Accessories,
    Deals,
    Electronics,
    Entertainment,
    Family,
    Hobbies,
    Garden,
    Housing,
    Vehicles
}

package com.app.postThrift.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JwtSecretKey {

    private final JwtConfig jwtConfig;

    @Autowired
    public JwtSecretKey(JwtConfig jwtConfig) {
        this.jwtConfig = jwtConfig;
    }

   /* @Bean
    public SecretKey secretKey() { return Keys.hmacShaKeyFor(jwtConfig.getSecretKey().getBytes());
    }*/
}

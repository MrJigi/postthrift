package com.app.postThrift.models;

import javax.persistence.*;

@Entity
@Table() //add name of table
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private userRoles name;

    public Role() {
    }

    public Role(userRoles name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public userRoles getName() {
        return name;
    }

    public void setName(userRoles name) {
        this.name = name;
    }
}

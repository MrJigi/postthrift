package com.app.postThrift.Managers;

import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.Repo.RoleDBH;
import com.app.postThrift.exceptions.InvalidDataException;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.Member;
import com.app.postThrift.models.MemberPrinciple;
import com.app.postThrift.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@Service
@Validated
public class MemberManager implements UserDetailsService {
private MemberDBH memberDbh;
private RoleDBH roleDBH;
private PasswordEncoder passwordEncoder;
private ProductDBH productDBH;
private ProductManager productManager;

    @Autowired
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Autowired
    public void setRoleDBH(RoleDBH roleDBH) {
        this.roleDBH = roleDBH;
    }

    @Autowired
    public void setMemberDbh(MemberDBH memberDBH) {
        this.memberDbh = memberDBH;
    }

    @Autowired
    public void setProductDBH(ProductDBH productDBH){this.productDBH = productDBH;}

    @Autowired
    public void setProductManager(ProductManager productManager) {
        this.productManager = productManager;
    }

    public void updateUserLogInDate(Member member){
        if(member != null){
            member.setLastLoginDateDisplay(LocalDate.now());
            memberDbh.save(member);
        }
    }

    public Member getMemberByUsername(String username){
        Member searchForUser = memberDbh.getByUsername(username);
        if (searchForUser != null){
            return searchForUser;
        }
        return null;
    }

    public Member getMemberById(int id) {
        Member searchedMember = memberDbh.getById(id);
        if (searchedMember != null){
            return searchedMember;
        }
        return null;

    }
    public Member createMember(@Valid Member member){
        if(member != null)
        {
            memberDbh.save(member);
            return member;
        }
        else{
            return null;
        }
    }

    public List<Member> getAllMembers(){
        return memberDbh.findAll();
    }


    public boolean deleteProductForMember(Integer productId, Integer memberId) throws ResourceNotFoundException, InvalidDataException{
        Member returnedMember = memberDbh.getById(memberId);
        if(returnedMember != null){
            Product product = productDBH.getById(productId);
            if(product!=null){
                if (product.getSeller().equals(returnedMember.getUsername())){
                    productDBH.deleteById(product.getId());
                    return true;
                }else{
                    throw new InvalidDataException("The given product does not belong to the given member");
                }
            }else{
                throw new ResourceNotFoundException("No product was found with the given id");
            }
        }else{
           throw new ResourceNotFoundException("No member was found with the given id");
        }
    }

    public Member updateMember(Member member, int id){
        Member updatableMember = memberDbh.getById(id);
        if(updatableMember != null){
            member.setId(updatableMember.getId());
            memberDbh.save(member);
            return member;
        }
        return null;
    }

    public boolean deleteMember(int memberId){
        Member deletedMember = memberDbh.getById(memberId);
        if(deletedMember != null){
            // before you delete the member delete all the products that the member created
            memberDbh.deleteById(memberId);
            return true;
        }
        else{
            return false;
        }
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Member loadedMember = memberDbh.getByUsername(username);
        if(loadedMember != null){
            return new MemberPrinciple(loadedMember);
        }
        throw new UsernameNotFoundException(String.format("Username %s not found", username));

    }


}

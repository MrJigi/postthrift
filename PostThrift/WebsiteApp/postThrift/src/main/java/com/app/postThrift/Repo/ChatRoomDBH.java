package com.app.postThrift.Repo;

import com.app.postThrift.Websockets.ChatRoom.ChatRoom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ChatRoomDBH extends JpaRepository<ChatRoom,Integer> {
    List<ChatRoom> findAllByChatId(String chatId);
    ChatRoom findBySenderIdAndRecieverId(Integer senderId, Integer receiverId);

}

package com.app.postThrift.jwt;

import com.app.postThrift.exceptions.HttpResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class JwtTokenVerifier extends OncePerRequestFilter {

//    private  final SecretKey secretKey;
    private final JwtConfig jwtConfig;

//    public JwtTokenVerifier(SecretKey secretKey, JwtConfig jwtConfig) {
//        this.secretKey = secretKey;
//        this.jwtConfig = jwtConfig;
//    }
    public JwtTokenVerifier( JwtConfig jwtConfig) {

        this.jwtConfig = jwtConfig;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        String authorizationHeader = request.getHeader(jwtConfig.getAuthorizationHeader());

        if (Strings.isNullOrEmpty(authorizationHeader) || !authorizationHeader.startsWith(jwtConfig.getTokenPrefix())){
            filterChain.doFilter(request,response);
            return;
        }
        String token = authorizationHeader.replace(jwtConfig.getTokenPrefix(),"");

        try {
//            Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(secretKey).build().parseClaimsJws(token);
            Jws<Claims> claimsJws = Jwts.parser().setSigningKey(jwtConfig.getSecretKey()).parseClaimsJws(token);

            Claims body = claimsJws.getBody();
            String username = body.getSubject();
            var authorities = (List<Map<String, String>>) body.get("authorities");

            Set<SimpleGrantedAuthority> simpleGrantedAuthories = authorities.stream().map(m -> new SimpleGrantedAuthority(m.get("authority")))
                    .collect(Collectors.toSet());

            Authentication authentication = new UsernamePasswordAuthenticationToken(username,null,simpleGrantedAuthories);

            SecurityContextHolder.getContext().setAuthentication(authentication);

        }
        catch (JwtException e){

            ObjectMapper mapper = new ObjectMapper();
            HttpResponse httpResponse = new HttpResponse(HttpServletResponse.SC_UNAUTHORIZED, HttpStatus.UNAUTHORIZED,"Invalid",e.getMessage());
            String httpResponseJson = mapper.writeValueAsString(httpResponse);
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            //response.getWriter().write(httpResponseJson);
            response.flushBuffer();

        }
        filterChain.doFilter(request,response);
    }
}

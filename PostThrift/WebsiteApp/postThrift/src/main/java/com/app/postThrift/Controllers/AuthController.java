package com.app.postThrift.Controllers;

import com.app.postThrift.Managers.MemberManager;
import com.app.postThrift.Repo.RoleDBH;
import com.app.postThrift.exceptions.HttpResponse;
import com.app.postThrift.jwt.JwtConfig;
import com.app.postThrift.jwt.RegistrationAuthorizationRequest;
import com.app.postThrift.jwt.UsernameAndPasswordAuthenticationRequest;
import com.app.postThrift.models.Member;
import com.app.postThrift.models.Role;
import com.app.postThrift.security.JwtResponse;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "api/auth")
public class AuthController {

    private AuthenticationManager authenticationManager ;
    private JwtConfig config;
    private PasswordEncoder passwordEncoder;
    private RoleDBH roleDbh;
    private MemberManager memberManager;

    public AuthController(AuthenticationManager authenticationManager, JwtConfig config, PasswordEncoder passwordEncoder, RoleDBH roleDbh, MemberManager memberManager) {
        this.authenticationManager = authenticationManager;
        this.config = config;
        this.passwordEncoder = passwordEncoder;
        this.roleDbh = roleDbh;
        this.memberManager = memberManager;
    }

    //    @RequestMapping(value = "/getByUsername/{username}",method = RequestMethod.GET)
//    public ResponseEntity GetByUsername(@PathVariable("Username") String username){
//        // memberManager.insertData();
//        if(memberManager.getMemberById(id)!= null){
//            return ResponseEntity.ok(memberManager.getMemberById(id));
//        }
//        else{
//            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Member was not found");
//        }
//    }

    @PostMapping("/signup")
    public ResponseEntity<Object> registerUser (@Valid @RequestBody RegistrationAuthorizationRequest registerRequest) {

        //create new member obj
        Member member = new Member();
        //set necessary fields and encode password
        member.setUsername(registerRequest.getUsername());
        member.setPassword(passwordEncoder.encode(registerRequest.getPassword()));
        member.setEmail(registerRequest.getEmail());
        member.setFirstName(registerRequest.getFirstName());
        member.setLastName(registerRequest.getLastName());
        //get roles from DB and assign to member obj
        Set<Role> roles = new HashSet<>();
        Role roleMember = roleDbh.getById(1);
        roles.add(roleMember);
        member.setRoles(roles);
        //save member obj to DB
        memberManager.createMember(member);
        HttpResponse responseBody = new HttpResponse(200, HttpStatus.OK,"Registration status","Registration was successful");
        return ResponseEntity.ok(responseBody);
    }


    @PostMapping("/signin")
    public ResponseEntity<Object> authenticateUser(@Valid @RequestBody UsernameAndPasswordAuthenticationRequest loginRequest) {

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);
        authentication.getName(); //MemberPrinciple

//
//        String token = Jwts.builder()
//                .setSubject(authentication.getName())
//                .claim("authorities", authentication.getAuthorities())
//                .setIssuedAt(new Date())
//                .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(config.getTokenExpirationAfterDays())))
//                .signWith(Keys.hmacShaKeyFor(config.getSecretKey().getBytes()))
//                .compact();
        String token = Jwts.builder()
                .setSubject(authentication.getName())
                .claim("authorities", authentication.getAuthorities())
                .setIssuedAt(new Date())
                .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(config.getTokenExpirationAfterDays())))
                .signWith(SignatureAlgorithm.HS512, config.getSecretKey())
                .compact();
        List<String> roles = authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
        roles = roles.stream().map(
                r -> {
                    if(r.equals("ROLE_MEMBER"))
                    {
                        return "MEMBER";
                    }
                    else if(r.equals("ROLE_ADMIN")){
                        return "ADMIN";
                    }
                    else
                    {
                        return "";
                    }

                }
        ).collect(Collectors.toList());
        Member customMember = memberManager.getMemberByUsername(loginRequest.getUsername());
        memberManager.updateUserLogInDate(customMember);
        JwtResponse jwtResponse = new JwtResponse(token,customMember.getId(),roles);

//      String token = Jwts.builder()
//                .setSubject(authentication.getName())
//                .claim("authorities", authentication.getAuthorities())
//                .setIssuedAt(new Date())
//                .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(config.getTokenExpirationAfterDays())))
//                .signWith(Keys.hmacShaKeyFor(config.getSecretKey().getBytes()))
//                .compact();


        return ResponseEntity.ok(jwtResponse);
    }
}

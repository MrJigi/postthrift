package com.app.postThrift.Websockets.Message;

public enum MessageStatus {
    RECEIVED,
    DELIVERED
}

package com.app.postThrift.Websockets.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ViewingController {
    @Autowired
    private SimpMessagingTemplate template;

    // Initialize Notifications
    private ViewingCounter notifications = new ViewingCounter(0);

    @GetMapping("/notify")
    public String getNotification() {

        // Increment Notification by one
        notifications.increment();

        // Push notifications to front-end
        template.convertAndSend("/topic/chat", notifications);

        return "Notifications successfully sent to Angular !";
    }
}

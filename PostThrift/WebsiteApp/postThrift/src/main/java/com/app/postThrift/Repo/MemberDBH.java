package com.app.postThrift.Repo;

import com.app.postThrift.models.Member;
import com.app.postThrift.models.Role;
import com.app.postThrift.models.userRoles;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MemberDBH extends JpaRepository<Member, Integer> {

    Member getById(Integer id);
//    long countAllByActiveEqualsAndRoles(boolean active, userRoles role);
    Optional<Member> getMemberByUsername(String username);
//    Optional<Member> findAllByIsActiveEquals(boolean active, userRoles role);
    Member getByUsername(String username);

//    Member findByUsername(String username);
    Member save(Member member);
    Member getByFirstName(String name);
}

package com.app.postThrift;


import com.app.postThrift.Repo.RoleDBH;
import com.app.postThrift.models.Role;
import com.app.postThrift.models.userRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
//@RestController
public class PostThriftApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(PostThriftApplication.class, args);
	}
	private RoleDBH roleDBH;

	@Autowired
	public void setRoleDBH(RoleDBH roleDBH) {
		this.roleDBH = roleDBH;
	}

	private void checkStoredRoles(){

		Role roleUser = roleDBH.getById(1);
		if (roleUser == null){
			roleUser = new Role();
			roleUser.setName(userRoles.ROLE_MEMBER);
			roleDBH.save(roleUser);
		}
		Role roleAdmin = roleDBH.getById(2);
		if (roleAdmin == null){
			roleAdmin = new Role();
			roleAdmin.setName(userRoles.ROLE_ADMIN);
			roleDBH.save(roleAdmin);
		}
	}

//	@Bean
//	public CorsFilter corsFilter(){
//		CorsConfiguration configuration = new CorsConfiguration();
//		configuration.setAllowedOrigins(Arrays.asList("http://localhost:4200"));
//		configuration.setAllowCredentials(true);
//		configuration.setAllowedHeaders(Arrays.asList("Origin",
//				"Access-Control-Allow-Origin",
//				"Content-Type",
//				"Accept",
//				"Authorization",
//				"Origin, Accept",
//				"Accept",
//				"Authorization",
//				"Origin, Accept",
//				"X-Requested-With",
//				"Access-Control-Request-Method",
//				"Access-Control-Request-Headers"));
//		configuration.setExposedHeaders(Arrays.asList("Origin","Content-Type","Accept","Authorization",
//				"Access-Control-Request-Method","Access-Control-Reqeust-Headers"));
//		configuration.setAllowedMethods(Arrays.asList("GET","POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
//           /* configuration.setExposedHeaders(Arrays.asList("Authorization", "content-type"));
//            configuration.setAllowedHeaders(Arrays.asList("Authorization", "content-type"));*/
//		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//		source.registerCorsConfiguration("/**", configuration);
//		return new CorsFilter(source);
//	}

	@Override
	public void run(String... args) throws Exception {
		checkStoredRoles();
      //  productManager.insertData();
//            memberManager.insertData();*/
		//memberManager.insertData();

	}
}

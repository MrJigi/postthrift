package com.app.postThrift.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NO_CONTENT)
public class NullReferencePointers extends NullPointerException {

    public NullReferencePointers() {
        super("The item is not set");
    }

    public NullReferencePointers(String message) {
        super(message);
    }
}

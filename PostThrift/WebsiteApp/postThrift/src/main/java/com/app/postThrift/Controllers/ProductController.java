package com.app.postThrift.Controllers;

import com.app.postThrift.Managers.ProductManager;
import com.app.postThrift.exceptions.InvalidDataException;
import com.app.postThrift.exceptions.ResourceNotFoundException;
//import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.AdvertiseRequest;
import com.app.postThrift.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path = "api/product")
public class ProductController {
    private ProductManager productManager;


    @Autowired
    public ProductController(ProductManager productManager) { this.productManager = productManager;}

    /*@RequestMapping(method = RequestMethod.GET)
    public ResponseEntity getAllProducts(){
        List<Product> productList = new ArrayList<Product>();
        productList = productManager.testData();
        return ResponseEntity.ok(productList);
       // productList =
    }*/

    @RequestMapping(value = "/{name}", method = RequestMethod.GET)
    public ResponseEntity getProductByName(@PathVariable("name") String name){
       // productManager.insertData();

        if(productManager.testIfExists(name) != null){
            return ResponseEntity.ok(productManager.testIfExists(name));
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Member with a name "+name+" was not found");
        }
    }

    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    public ResponseEntity getAllProducts(){
        if(productManager.getAllProducts()!= null){
            return ResponseEntity.ok(productManager.getAllProducts());
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("No products found");
        }
    }
    @GetMapping(value = "/getAllCategory")
    public ResponseEntity getAllCategorys(){
        if(productManager.getAllCategory() != null){
            return ResponseEntity.ok(productManager.getAllCategory());
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Couln't retrieve categories");
        }
    }

    @RequestMapping(path = "/GetId/{id}", method = RequestMethod.GET)
    public ResponseEntity getProductById(@PathVariable(name = "id") Integer id){
        /* productManager.insertData();*/

        if(productManager.getProductById(id) != null){
            return ResponseEntity.ok(productManager.getProductById(id));
        }
        else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Member with this" + id + "does not exist");
        }
    }

 /*  @RequestMapping(path = "/CreateMember/{name},{price},{description},{date}, {city}", method = RequestMethod.POST)
    public ResponseEntity CreateProduct(@PathVariable())*/

    @RequestMapping(method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE)
//    @PostMapping("/newProduct/{name},{price},{description},{date},{city}")
    public ResponseEntity<Product> createProduct (@RequestBody AdvertiseRequest advert) throws InvalidDataException {
            Product product1 = productManager.createProduct(advert.getProduct(),advert.getSellerUsername()); //seller username
            return new ResponseEntity<>(product1,HttpStatus.CREATED);

    }
    // @RequestMapping(method = RequestMethod.DELETE,consumes = MediaType.APPLICATION_JSON_VALUE)
    @DeleteMapping(path = "/removeProduct/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<Object> delete(@PathVariable("id") Integer id) throws ResourceNotFoundException {
        Product product1 = productManager.getProductById(id) ;
        if (product1 != null) {
            productManager.deleteProduct(product1);
            return ResponseEntity.ok().build();
        }
        else{
            throw new ResourceNotFoundException("No product was found with the given id");
        }
    }
    @PutMapping(path = "/updateProduct/{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable("id") Integer id, @RequestBody Product product) {
        productManager.updateProduct(product,id);
        Product updatableProduct =  productManager.updateProduct(product,id);
        if ( updatableProduct != null){
            return ResponseEntity.ok(product);
        }
        else{
            throw new ResourceNotFoundException("Update was failed");
        }

    }



}

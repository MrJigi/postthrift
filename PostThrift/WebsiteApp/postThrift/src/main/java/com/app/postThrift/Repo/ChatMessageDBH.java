package com.app.postThrift.Repo;

import com.app.postThrift.Websockets.Message.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ChatMessageDBH extends JpaRepository<Message,Integer> {
    Message getByChatId(Integer chatId);
    List<Message> findAllByChatId(String chatId);
    List<Message> findAllBySenderIdAndReceiverId(Integer senderId,Integer receiverId);
    Message getByMessageId(Integer messageId);

}

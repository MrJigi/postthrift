package com.app.postThrift.Websockets.ChatRoom;

import com.app.postThrift.Repo.ChatMessageDBH;
import com.app.postThrift.Repo.ChatRoomDBH;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Websockets.Message.Message;
import com.app.postThrift.Websockets.Message.MessageStatus;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class MessagingService {
    private MemberDBH memberRepo;
    private ChatMessageDBH messagingRepo;
    private ChatRoomDBH chatRoomRepo;


    public MessagingService(MemberDBH memberRepo, ChatMessageDBH messagingRepo, ChatRoomDBH chatRoomRepo) {
        this.memberRepo = memberRepo;
        this.messagingRepo = messagingRepo;
        this.chatRoomRepo = chatRoomRepo;
    }

    public Message save(Message sentMessage){
        sentMessage.setMessageStatus(MessageStatus.DELIVERED);
        messagingRepo.save(sentMessage);
        return sentMessage;
    }

    public List<Message> searchForMessages(Integer senderId,Integer receiverId)
    {
      String chatId = chatRoomRepo.findBySenderIdAndRecieverId(senderId,receiverId).getChatId();
//      chatRoomService.GetChatId(senderId,receiverId);
       List<Message> chatMessages = messagingRepo.findAllByChatId(chatId) ;
       if(!chatMessages.isEmpty())
       {
           statusUpdate(senderId,receiverId,MessageStatus.DELIVERED);
       }
       return chatMessages;
    }

    public void statusUpdate(Integer senderId, Integer receiverId,MessageStatus status){
        List<Message> messageList = messagingRepo.findAllBySenderIdAndReceiverId(senderId,receiverId).stream().map(
               message -> {
                   message.setMessageStatus(status);
                   return message;
               }
        ).collect(Collectors.toList());
        messagingRepo.saveAll(messageList);
    }

    public Message findById(Integer messageId) throws ResourceNotFoundException {
        Message searchedMessage = messagingRepo.getByMessageId(messageId);
        if(searchedMessage!= null){
            searchedMessage.setMessageStatus(MessageStatus.DELIVERED);
            return messagingRepo.save(searchedMessage);
        }
        else
        {
            throw new ResourceNotFoundException("Chat message does not exist with that id");
        }
    }
}


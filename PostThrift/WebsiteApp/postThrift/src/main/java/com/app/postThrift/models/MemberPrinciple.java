package com.app.postThrift.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

public class MemberPrinciple implements UserDetails {
    private Member member;

    public MemberPrinciple(Member member) {
        this.member = member;
    }

//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return new Member("admin","password",new ArrayList<>());
//    }

   /* @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        return stream(this.member.getRoles()).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }
*/

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.member.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName().name())).collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return member.getPassword();
    }

    @Override
    public String getUsername() {
        return member.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return this.member.isActive();
    }


  /*  @Override
    public String getAuthority() {
        return this.member.getRoles();
    }*/
}

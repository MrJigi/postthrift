package com.app.postThrift.Websockets.Message;

public class ViewingCounter {
    private int count;

    public ViewingCounter(int count) {
        this.count = count;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
    public void increment() {
        this.count++;
    }
}

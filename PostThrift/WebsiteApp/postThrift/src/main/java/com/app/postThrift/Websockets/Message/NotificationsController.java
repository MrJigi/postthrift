package com.app.postThrift.Websockets.Message;

import com.app.postThrift.Managers.NotificationManager;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Websockets.ChatRoom.ChatRoomService;
import com.app.postThrift.Websockets.ChatRoom.MessagingService;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Controller
public class NotificationsController {
//    private final NotificationDispatcher dispatcher;

    private NotificationManager notificationManager;
    private SimpMessagingTemplate template;
    private Logger logger= LoggerFactory.getLogger(NotificationsController.class);

    private ChatRoomService chatRoomService;
    private MessagingService messagingService;
    private MemberDBH memberDBH;

    public NotificationsController(NotificationManager notificationManager, SimpMessagingTemplate template, ChatRoomService chatRoomService, MessagingService messagingService, MemberDBH memberDBH) {
        this.notificationManager = notificationManager;
        this.template = template;
        this.chatRoomService = chatRoomService;
        this.messagingService = messagingService;
        this.memberDBH = memberDBH;
    }

    //    @Autowired
//    public NotificationsController(NotificationDispatcher dispatcher) {
//        this.dispatcher = dispatcher;
//    }




    //This one works
//    @MessageMapping("/chat")
//    @SendTo("/topic/news")
//    public Notification sendMessage(@Payload final Notification message) {
//        return message;
//    }

//    @MessageMapping("/chat")
//    @SendTo("/topic/news")
//    public void sendMessageViaDispatcher(@Payload Message message) {
//        Notification notification = new Notification();
//        notification.setTime(LocalDateTime.now().toString());
//        notification.setSender("test");
//        notification.setText(message.getContent());
//        notification.setType(MessageType.CHAT);
//        this.template.convertAndSend("/notifications",notification);
//    }

//    @MessageMapping("/chat")
//   // @SendTo("queue/news")
//    @SendTo("/topic/notifications")
//    public Message sentMessage(String message) throws Exception {
//        Thread.sleep(1000);
//        return new Message("This message contains: " + message);
//    }

    //    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @GetMapping("api/messages/{id}")
    public ResponseEntity<Object> findMessage(@PathVariable(name = "id") int id) throws ResourceNotFoundException {
        return ResponseEntity.ok(messagingService.findById(id));
    }

    //    @PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
    @GetMapping("api/messages/{senderId}/{recipientName}")
    public ResponseEntity<Object> findChatMessages (@PathVariable Integer senderId,
                                                    @PathVariable String recipientName) {
        Member reciever = memberDBH.getByUsername(recipientName);
        return ResponseEntity.ok(messagingService.searchForMessages(senderId, reciever.getId()));
    }

    @MessageMapping("/chat")
    public void sendMessage(@Payload Message message) throws Exception {

        Member senderMember = memberDBH.getByUsername(message.getSenderName());
        Member receiverMember = memberDBH.getByUsername(message.getReceiverName());

        if(senderMember != null && receiverMember != null){
        message.setSenderId(senderMember.getId());
        message.setReceiverId(receiverMember.getId());
            String chatId = chatRoomService.GetChatId(message.getSenderId(),message.getReceiverId());
        if(chatId == null){
         chatId = String.format("%s_%s",message.getSenderId(),message.getReceiverId());
         chatRoomService.createRoom(chatId,message.getSenderId(),message.getReceiverId());
        }
        message.setChatId(chatId);
        message.setMessageTimeStamp(LocalDateTime.now());
        Message returnedMessage = messagingService.save(message);
//        notificationManager.createAndSendNotification(returnedMessage.getReceiverId().toString(),returnedMessage);
        template.convertAndSendToUser(returnedMessage.getReceiverId().toString(),"/queue/notifications",new Notification(message.getContent(),message.getSenderName(), LocalDate.now().toString(),message.getMessageId()));
        }else{
            logger.error("No sender or receiver to send a message");
        }
    }



//    @MessageMapping("/chat.newUser")
//    @SendTo("/topic/public")
//    public Notification newUser(@Payload final Notification message,
//                                SimpMessageHeaderAccessor headerAccessor) {
//        headerAccessor.getSessionAttributes().put("username",message.getSender());
//        return message;
//    }
//    @MessageMapping("/news")
//    public void brodcastNewsOtherWay(@Payload String message) {
//        this.simpMessagingTemplate.convertAndSend("/topic/news", message)
//    }

//    @MessageMapping("hello")
//    @SendTo("/topic/greetings")
//    public Notification notification (String message) throws Exception {
//        Thread.sleep(1000);
//        return new Notification();
//    }
    //These two work

//    @MessageMapping("/start")
//    public void start(StompHeaderAccessor stompHeaderAccessor) {
//        dispatcher.add(stompHeaderAccessor.getSessionId());
//    }
//    @MessageMapping("/stop")
//    public void stop(StompHeaderAccessor stompHeaderAccessor) {
//        dispatcher.remove(stompHeaderAccessor.getSessionId());
//    }

}

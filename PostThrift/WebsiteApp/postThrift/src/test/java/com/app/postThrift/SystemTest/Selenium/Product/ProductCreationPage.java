//package com.app.postThrift.SystemTest.Selenium.Product;
//
//import org.openqa.selenium.*;
//import org.openqa.selenium.firefox.FirefoxDriver;
//
//import java.time.LocalDate;
//
//public class ProductCreationPage {
//    private WebDriver driver;
//    private int waitTime = 3000;
//    private String url = "http://localhost:4200/#/CreateProduct";
//
//    public WebDriver getDriver() {
//        return driver;
//    }
//
//    public ProductCreationPage() {
//        System.setProperty("webdriver.gecko.driver", "C:\\Gecko\\geckodriver.exe");
//        //System.setProperty("webdriver.chrome.driver", "C:\\Chrome-Selenium-drivers\\chromedriver.exe");
//        driver = new FirefoxDriver();
//        driver.manage().window().maximize();
//        driver.get(url);
//    }
//
////    public void createProduct(){
////
////
////    }
////    public void submitProductCreateForm(){
////
////    }
////    public void acceptAlert(){
////
////    }
////    public void travelToProductListingByNavbar(){
//
//  //  }
//    public void travelToProductListingByNavbar() throws InterruptedException {
//        // initialise the js executor
//        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
//        // scroll to top to be sure the navbar is into view
//        jse.executeScript("window.scrollTo({ top: 0, behavior: 'smooth' });");
//        Thread.sleep(1000);
//        // click the nav link for the listing
//        String jsScript = "let navCreationBtn = document.querySelector(\"ul.navbar-nav li.nav-item:nth-child(4) a\");" +
//                "navCreationBtn.click();";
//        jse.executeScript(jsScript);
//    }
//
//    public void travelToProductCreation(){
//        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
//        String jsScript = "let navCreationBtn = document.querySelector(\"ul.navbar-nav li.nav-item:nth-child(3) a\");" +
//                "navCreationBtn.click();";
//        jse.executeScript(jsScript);
//    }
//
//    public void createProduct(String name, String price, String desc, String city) throws InterruptedException {
//        // create the javaScript executor
//        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
//        //get inputs and add text
//        // name
//        WebElement nameInput = driver.findElement(By.id("name"));
//        nameInput.sendKeys(name);
//        Thread.sleep(500);
//        // price
//        WebElement priceInput = driver.findElement(By.id("price"));
//        priceInput.sendKeys(price);
//        Thread.sleep(500);
//        // description
//        WebElement descInput = driver.findElement(By.id("description"));
//        descInput.sendKeys(desc);
//        Thread.sleep(500);
//        // city
//        WebElement cityInput = driver.findElement(By.id("city"));
//        cityInput.sendKeys(city);
//        // scroll to the category select otherwise the select is not clickable as it is cover by another element
//        WebElement productCategory = driver.findElement(By.id("category"));
//        productCategory.click();
//        Thread.sleep(1000);
//        //WebElement productCategorySelector = driver
//        //productCategory.
//        productCategory.findElements(By.linkText("Accessories"));
//        productCategory.click();
//        Thread.sleep(1000);
//        // select an option
//        WebElement categorySelectOption = driver.findElement(By.id("category")).findElement(By.xpath("//option[@value='Family']"));
//        categorySelectOption.click();
//        Thread.sleep(1000);
//
//        WebElement productSubmit = driver.findElement(By.id("submit"));
//        productSubmit.isSelected();
//    }
//
//    public void submitProductCreateForm(){
//        // create the javaScript executor
//        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
//        // writing js script to get the select option and click it
//        String jsScript = "let submitBtn = document.querySelector(\"div.list-group-horizontal form button[type='submit']\");" +
//                "submitBtn.click();";
//        jse.executeScript(jsScript);
//    }
//
//    public void acceptAlert(){
//        driver.switchTo().alert().accept();
//    }
//
//
//
//    public boolean checkIfProductIsCreated(String name, String price, String description, LocalDate creationDate, String city, String category){
//
//        try {
//            //name
//            WebElement productName = driver.findElement(By.id("name"));
//            productName.sendKeys("NameTest");
//
//            WebElement productPrice = driver.findElement(By.id("price"));
//            productPrice.sendKeys("2");
//
//            WebElement productDescription = driver.findElement(By.id("description"));
//            productDescription.sendKeys("descriptionTest");
//
//            WebElement productCity = driver.findElement(By.id("city"));
//            productCity.sendKeys("Eindhoven");
//
//            WebElement productCategory = driver.findElement(By.id("category"));
//            productCategory.click();
//            //WebElement productCategorySelector = driver
//            //productCategory.
//            productCategory.findElements(By.linkText("Accessories"));
//            productCategory.click();
//           // productCategory.sendKeys("Accessories");
//
//            WebElement productSubmit = driver.findElement(By.id("submit"));
//            productSubmit.isSelected();
//            return true;
//        }
//        catch (NoSuchElementException e){
//            return false;
//        }
//    }/*
//        this.name = name;
//        this.price = price;
//        this.description = description;
//        this.creationDate = creationDate;
//        this.city = city;
//        this.category = category;*/
//
//    public void closeWindow() {
//        this.driver.close();
//    }
//
//    public void closeBrowser() {
//        this.driver.quit();
//    }
//
//    public int getWaitTime() {
//        return waitTime;
//    }
//
//    public void setWaitTime(int waitTime) {
//        this.waitTime = waitTime;
//    }
//}

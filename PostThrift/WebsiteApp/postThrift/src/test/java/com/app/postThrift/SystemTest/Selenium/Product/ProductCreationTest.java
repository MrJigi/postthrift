//package com.app.postThrift.SystemTest.Selenium.Product;
//
//import com.app.postThrift.Repo.ProductDBH;
//import com.app.postThrift.models.CategoryType;
//import com.app.postThrift.models.Product;
//import org.junit.jupiter.api.BeforeEach;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.annotation.DirtiesContext;
//import org.springframework.test.context.TestPropertySource;
//
//import java.time.LocalDate;
//
//@TestPropertySource(locations = "classpath:application-test.properties")
//@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
//public class ProductCreationTest {
//    private ProductCreationPage productCreationPage;
//
//    @Autowired
//    private ProductDBH productDBH;
//
//    @BeforeEach
//    public void setUp(){
//        Product product = new Product(
//                "Iphone",
//                "80",
//                "Haven't used it in a while because i bought a new one so selling this one.",
//                LocalDate.now(),
//                "Eindhoven",
//                CategoryType.Electronics);
//        productDBH.save(product);
//    }
//// not sure if work on checkIfProductIsCreated bcz of category
////    @Test
////    public void productCreationTest() throws Exception {
////        //arrange
////        productCreationPage = new ProductCreationPage();
////        Thread.sleep(2000);
////        //act
////        boolean result = productCreationPage.checkIfProductIsCreated("Test","10","Description test", LocalDate.now(),"TestCity", CategoryType.Accessories.name());
////        Thread.sleep(1000);
////        //assert
////        productCreationPage.closeBrowser();
////        assertTrue(result);
////    }
//
//
//
////    @Test
////    public void createProductTest() throws Exception{
////        //arrange
////
////        productCreationPage = new ProductCreationPage();
////        ProductListingPage pd = new ProductListingPage(productCreationPage.getDriver());
////        Thread.sleep(1000);
////        //act
////        productCreationPage.createProduct("hemjy", "22", "none", "rotterdam");
////        Thread.sleep(1000);
////        productCreationPage.submitProductCreateForm();
////        Thread.sleep(2000);
////        productCreationPage.acceptAlert();
////        //assert
////        productCreationPage.travelToProductListingByNavbar();
////        Thread.sleep(2000);
////        boolean result = pd.checkIfProductExists(2);
////        productCreationPage.closeBrowser();
////        assertTrue(result);
////    }
//
//
//
//}

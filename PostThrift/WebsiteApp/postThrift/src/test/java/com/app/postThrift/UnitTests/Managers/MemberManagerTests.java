package com.app.postThrift.UnitTests.Managers;

import com.app.postThrift.IntegrationTest.AbstractTest;
import com.app.postThrift.Managers.MemberManager;
import com.app.postThrift.Managers.ProductManager;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.Repo.RoleDBH;
import com.app.postThrift.exceptions.InvalidDataException;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.util.ReflectionTestUtils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-test.properties")
public class MemberManagerTests extends AbstractTest {

  public Member testMember(){
      Role memberRole = new Role(userRoles.ROLE_MEMBER);
      Set<Role> roles = new HashSet<>();
      roles.add(memberRole);
      Member testMember = new Member("Rose",
              "Nyland",
              "user",
              "password",
              "SaintOlaf@gmail.com",
              LocalDate.of(1980,4,5),
              "USA",
              "SaintOlaf",
              "UrlOfPic",
              roles,
              LocalDate.now(),
              true );
      testMember.setId(1);
      return testMember;
  }
    public Product testProduct(){
        Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
        product.setId(1);
        product.setSeller(testMember().getUsername());
        return product;
    }
    public Member testMemberAdmin(){
        Role memberRole = new Role(userRoles.ROLE_ADMIN);
        Set<Role> roles = new HashSet<>();
        roles.add(memberRole);
        Member testAdmin = new Member("Dorothy",
                "Zborznak",
                "username",
                "password",
                "Dorothy@gmail.com",
                LocalDate.of(1980,4,5),
                "USA",
                "Mayami",
                "UrlOfPic",
                roles,
                LocalDate.now(),
                true );
        testAdmin.setId(2);
        return testAdmin;
    }
    @Mock
    private ProductDBH productDBH;
  @Mock
  private MemberDBH memberDbh;
  @Mock
  private RoleDBH roleDB;
  @InjectMocks
  private MemberManager memberManager;
  @InjectMocks
  private ProductManager productManager;
  @BeforeAll
  public void setup() throws Exception{
      MockitoAnnotations.openMocks(this);
      ReflectionTestUtils.setField(memberManager, "passwordEncoder", new BCryptPasswordEncoder());
  }

  @Test
  void loadUserByUsernameTest() throws Exception{
      // arrange
      Mockito.when(memberDbh.getByUsername("user")).thenReturn(testMember());
      // act
      UserDetails returnedPrinciple = memberManager.loadUserByUsername("user");
      // assert
      assertNotNull(returnedPrinciple);
  }

  @Test
  void loadUserByUsernameWithNoUserFoundTest() throws Exception{
      Mockito.when(memberDbh.getByUsername("user")).thenReturn(null);
      assertThrows(UsernameNotFoundException.class, ()->{
          memberManager.loadUserByUsername("user");
      });
  }

  @Test
  void getMemberByUsernameTest() throws Exception{
      Mockito.when(memberDbh.getByUsername("user")).thenReturn(testMember());
      Member returnedMember = memberManager.getMemberByUsername("user");
      assertNotNull(returnedMember);
  }

  @Test
  void getMemberByUsernameWithNoMemberFoundTest() throws Exception{
      Mockito.when(memberDbh.getByUsername("user")).thenReturn(null);
      Member returnedMember = memberManager.getMemberByUsername("user");
      assertNull(returnedMember);
  }

  @Test
  void getMemberByIdTest() throws Exception{
      Mockito.when(memberDbh.getById(1)).thenReturn(testMember());
      Member returnedMember = memberManager.getMemberById(1);
      assertNotNull(returnedMember);
  }

  @Test
  void getMemberByIdWithNoMemberFoundTest() throws Exception{
      Mockito.when(memberDbh.getById(1)).thenReturn(null);
      Member returnedMember = memberManager.getMemberById(1);
      assertNull(returnedMember);
  }

  @Test
  void createMemberTest() throws Exception{
      Member returnedMember = memberManager.createMember(testMember());
      assertNotNull(returnedMember);
  }

  @Test
  void createMemberWithInvalidMemberObjTest() throws Exception{
      Member returnedMember = memberManager.createMember(null);
      assertNull(returnedMember);
  }

  @Test
  void getAllMembersTest() throws Exception{
      List<Member> members = new ArrayList<>();
      members.add(testMember());
      Mockito.when(memberDbh.findAll()).thenReturn(members);
      List<Member> returnedMembers = memberManager.getAllMembers();
      assertEquals(1, returnedMembers.size());
  }

  @Test
  void updateMemberTest() throws Exception{
      Mockito.when(memberDbh.getById(1)).thenReturn(testMember());
      Member returnedMember = memberManager.updateMember(testMember(), 1);
      assertNotNull(returnedMember);
  }

  @Test
  void updateMemberWithInvalidMemberIdTest() throws Exception{
      Mockito.when(memberDbh.getById(1)).thenReturn(null);
      Member returnedMember = memberManager.updateMember(testMember(), 1);
      assertNull(returnedMember);
  }

    @Test
    void deleteMembersProduct() throws Exception{
      Mockito.when(memberDbh.getById(1)).thenReturn(testMember());
      Mockito.when(productDBH.getById(1)).thenReturn(testProduct());
     boolean result = memberManager.deleteProductForMember(testProduct().getId(), testMember().getId());
      assertTrue(result);
  }
    @Test
    void deleteMemberProductWithInvalidMember() throws Exception {
        Mockito.when(memberDbh.getById(1)).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            memberManager.deleteProductForMember(1,1);
        });
    }

    @Test
    void deleteMemberProductWithInvalidProduct() throws Exception {
        Mockito.when(memberDbh.getById(1)).thenReturn(testMember());
        Mockito.when(productDBH.getById(1)).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            memberManager.deleteProductForMember(1,testMember().getId());
        });
    }

    @Test
    void deleteMembersProductWithInvalidSeller() throws Exception{
        Product testProduct2 = testProduct();
        testProduct2.setSeller("Brenda");
        Mockito.when(memberDbh.getById(1)).thenReturn(testMember());
        Mockito.when(productDBH.getById(1)).thenReturn(testProduct2);
        Exception exception = assertThrows(InvalidDataException.class, () -> {
            memberManager.deleteProductForMember(testProduct2.getId(), testMember().getId());
        });
    }

    @Test
    void deleteMemberByIdTest() throws Exception{
      Mockito.when(memberDbh.getById(1)).thenReturn(testMember());
      boolean result = memberManager.deleteMember(1);
      assertTrue(result);
    }

    @Test
    void deleteMemberByIdWithNoMemberFoundTest() throws Exception{
        Mockito.when(memberDbh.getById(1)).thenReturn(null);
        boolean result = memberManager.deleteMember(1);
        assertFalse(result);
    }


//    @Test
//    void createProductWithInvalidProductTest() throws Exception{
//        Exception exception = assertThrows(InvalidDataException.class, () -> {
//            Product createdProduct = productManager.createProduct(null,testMember.getUsername());
//        });
//    }
//
//    @Test
//    void createProductWithNoMemberFoundTest() throws Exception{
//        Mockito.when(memberManager.getMemberByUsername("user")).thenReturn(null);
//        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
//            Product createdProduct = productManager.createProduct(product,testMember.getUsername());
//        });
//    }
}

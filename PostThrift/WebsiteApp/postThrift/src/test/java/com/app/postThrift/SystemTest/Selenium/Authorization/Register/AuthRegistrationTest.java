package com.app.postThrift.SystemTest.Selenium.Authorization.Register;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuthRegistrationTest {
    @Test
    public void LogInTest() throws Exception {
        //arrange
        AuthRegistrationPage registrationInPage = new AuthRegistrationPage();
        Thread.sleep(2000);
        registrationInPage.changeToRegister();

        //act
        boolean result = registrationInPage.checkIfTextBoxIsThere();
        assertTrue(result);
        // boolean logInResult = registrationInPage.logInTest("user","password");
        Thread.sleep(1000);
        //assert
        registrationInPage.registrationTest("NamedFirst","LastNamed","UniqueUser","UniquePass","UniqueEmail@gmail.com");
        // registrationInPage.submitLogInForm();


        registrationInPage.closeBrowser();

    }

}

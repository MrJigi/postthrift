package com.app.postThrift.SystemTest.Selenium.Authorization.LogIn;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AuthLogInPage {


    private WebDriver driver;
    private int waitTime = 3000;
    private String url = "http://localhost:4200/#/LogInComponent";

//    static {
//        System.setProperty("webdriver.gecko.driver", "C:\\Gecko\\geckodriver.exe");
//    }


    public AuthLogInPage() {
        System.setProperty("webdriver.gecko.driver", "C:\\Gecko\\geckodriver.exe");
        //System.setProperty("webdriver.chrome.driver", "C:\\Chrome-Selenium-drivers\\chromedriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(url);


    }

    public AuthLogInPage(WebDriver webDriver,int waitTime) {
        this.waitTime = waitTime;
        this.driver = webDriver;
    }

    public void travelToLogInByNavbar() throws InterruptedException {
        // initialise the js executor
        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
        // scroll to top to be sure the navbar is into view
        jse.executeScript("window.scrollTo({ top: 0, behavior: 'smooth' });");
        Thread.sleep(1000);
        // click the nav link for the listing
        String jsScript = "let navCreationBtn = document.querySelector(\"ul.navbar-nav li.nav-item:nth-child(6) a\");" +
                "navCreationBtn.click();";
        jse.executeScript(jsScript);
    }

    public boolean checkIfTextBoxIsThere(){
        String usernameBox = "username";
        String passwordBox = "password";
        try {

            WebElement UsernameBox = driver.findElement(By.id(usernameBox));
            WebElement PasswordBox = driver.findElement(By.id(passwordBox));
            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

    public void logInTest(String username, String password) throws InterruptedException {

        // create the javaScript executor
        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
        //get inputs and add text
        // name
        WebElement nameInput = driver.findElement(By.id("username"));
        nameInput.sendKeys(username);
        Thread.sleep(500);
        // price
        WebElement priceInput = driver.findElement(By.id("password"));
        priceInput.sendKeys(password);
        Thread.sleep(500);

        WebElement submit = driver.findElement(By.id("logInSubmit"));
        submit.submit();
        Thread.sleep(5000);
        //Thread.sleep(500);

    }

    public void submitLogInForm(){
        // create the javaScript executor
        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
        // writing js script to get the select option and click it
        String jsScript = "let logIn-submit = document.querySelector(\"div.list-group-horizontal form button[type='submit']\");" +
                "logInBtn.click();";
        jse.executeScript(jsScript);
    }





    public AuthLogInPage(WebDriver driver) {
        this.driver = driver;
    }
    public void closeWindow() { this.driver.close();}
    public void closeBrowser() {this.driver.quit();}

    public int getWaitTime() {
        return waitTime;
    }
    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }
}

//
//package com.app.postThrift.IntegrationTest;
//
//import com.app.postThrift.Repo.MemberDBH;
//import com.app.postThrift.Repo.ProductDBH;
//import com.app.postThrift.models.*;
//import com.fasterxml.jackson.core.JsonParseException;
//import com.fasterxml.jackson.core.JsonProcessingException;
//import com.fasterxml.jackson.databind.JsonMappingException;
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.SerializationFeature;
//import org.junit.jupiter.api.*;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.TestPropertySource;
//import org.springframework.test.context.web.WebAppConfiguration;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.MvcResult;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import java.io.IOException;
//import java.time.LocalDate;
//import java.util.Arrays;
//import java.util.HashSet;
//import java.util.List;
//import java.util.Set;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.junit.jupiter.api.Assertions.assertNotNull;
//@SpringBootTest
//@WebAppConfiguration
//@TestPropertySource(locations = "classpath:application-test.properties")
//public class ProductController2Test {
//    private Logger log = LoggerFactory.getLogger(ProductController2Test.class);
//    private static MockMvc mvc;
//    private static Product testProduct;
//    @Autowired
//    private static MemberDBH memberDBH;
//    @Autowired
//    private static ProductDBH productDBH;
//    @Autowired
//    static WebApplicationContext webApplicationContext;
//
//    public String mapToJson(Object obj) throws JsonProcessingException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        return objectMapper.writeValueAsString(obj);
//    }
//
//    public <T> T mapFromJson(String json, Class<T> clazz) throws JsonParseException, JsonMappingException, IOException {
//        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.findAndRegisterModules().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
//        return objectMapper.readValue(json, clazz);
//    }
//
//    @BeforeAll
//    public static void setUp(){
//        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
//        Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
//        Product product2 = new Product("Iphone1","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
//
//        Role memberRole = new Role(userRoles.ROLE_MEMBER);
//        memberRole.setId(1);
//        Set<Role> roles = new HashSet<>();
//        roles.add(memberRole);
//        Member testMember = new Member("Rose",
//                "Nyland",
//                "user",
//                "test",
//                "SaintOlafinrerere@gmail.com",
//                LocalDate.of(1980,4,5),
//                "USA",
//                "SaintOlaf",
//                "UrlOfPic",
//                roles,
//                LocalDate.now(),
//                true );
//        product.setSeller(testMember.getUsername());
//        testProduct = product;
//        productDBH.save(product);
//        productDBH.save(product2);
//        memberDBH.save(testMember);
//    }
//
//    @AfterAll
//     public static void cleanup(){
//        memberDBH.deleteById(1);
//    }
//
//    @Test
//    public void getByIdTest() throws Exception{
//        // arrange
//        String endpoint = "http://locahost:8080/api/product/GetId/1";
//        // act
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
//        // assert
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200, status);
//        String content = mvcResult.getResponse().getContentAsString();
//        Product returnedProduct = this.mapFromJson(content, Product.class);
//        assertNotNull(returnedProduct);
//
//        //test
//    } //can add if its not found
//
//    @Test
//    public void getAllProductsTest() throws Exception{
//        String endpoint = "http://locahost:8080/api/product/getAll";
//        //act
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200,status);
//        String content = mvcResult.getResponse().getContentAsString();
//        List<Product> products = Arrays.asList(this.mapFromJson(content,Product[].class));
//        assertEquals(1, products.size());
//    }
//
//    @Test
//    public void getProductByNameTest() throws Exception {
//        String endpoint = "http://locahost:8080/api/product/Iphone";
//
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200,status);
//        String content = mvcResult.getResponse().getContentAsString();
//        Product product = this.mapFromJson(content,Product.class);
//        assertNotNull(product);
//    }
//
//    @Test
//    public void getAllCategories() throws Exception {
//        String endpoint = "http://locahost:8080/api/product/getAllCategory";
//
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200,status);
//        String content = mvcResult.getResponse().getContentAsString();
//        List<CategoryType> listOfCategories = Arrays.asList(this.mapFromJson(content,CategoryType[].class));
//        assertEquals(11,listOfCategories.size());
//    }
//
//    @Test
//    public void createProductTest() throws Exception {
//        String endpoint = "http://locahost:8080/api/product";
//        AdvertiseRequest adv = new AdvertiseRequest();
//        testProduct.setCreationDate(null);
//        adv.setProduct(testProduct);
//        adv.setSellerUsername("user");
//        String testProdRequestJson = this.mapToJson(adv);
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(endpoint).contentType(MediaType.APPLICATION_JSON_VALUE).content(testProdRequestJson).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(201,status);
//        String content = mvcResult.getResponse().getContentAsString();
//        Product product = this.mapFromJson(content,Product.class);
//        assertNotNull(product);
//    }
//
//    @Test
//    public void deleteProductTest() throws Exception {
//        String endpoint = "http://locahost:8080/api/product/removeProduct/2";
//
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(endpoint).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200,status);
//
//    }
//    @Test
//    public void updateProductTest() throws Exception {
//        String endpoint = "http://locahost:8080/api/product/updateProduct/1";
//        testProduct.setCreationDate(null);
//        String testProdRequestJson = this.mapToJson(testProduct);
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(endpoint).contentType(MediaType.APPLICATION_JSON_VALUE).content(testProdRequestJson).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200,status);
//        String content = mvcResult.getResponse().getContentAsString();
//        Product product = this.mapFromJson(content,Product.class);
//        assertNotNull(product);
//    }
//
//}
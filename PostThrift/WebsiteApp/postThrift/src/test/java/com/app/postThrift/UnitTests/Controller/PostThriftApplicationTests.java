package com.app.postThrift.UnitTests.Controller;

import com.app.postThrift.Controllers.ProductController;
import com.app.postThrift.Managers.ProductManager;
import com.app.postThrift.models.CategoryType;
import com.app.postThrift.models.Product;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;


//@SpringBootTest
//@ExtendWith(MockitoExtension.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PostThriftApplicationTests {
	@Mock private ProductManager productManager;
	//ProductManager productManager ;
	@InjectMocks
	ProductController productController ;

	@BeforeAll
	public void setUp(){
		MockitoAnnotations.openMocks(this);
		Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
		Mockito.when(productManager.testIfExists("Iphone")).thenReturn(product);
	}
	@Test
	void getProductByName() {
		ResponseEntity controllerResponse = productController.getProductByName("Iphone");
		Integer responseEntityStatus = controllerResponse.getStatusCodeValue();
		assertEquals(200,responseEntityStatus);
	}



}

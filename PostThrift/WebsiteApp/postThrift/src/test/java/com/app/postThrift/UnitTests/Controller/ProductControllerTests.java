package com.app.postThrift.UnitTests.Controller;

import com.app.postThrift.IntegrationTest.AbstractTest;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.models.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ProductControllerTests extends AbstractTest {

    private Product testProduct;
    @Autowired
    private MemberDBH memberDBH;
    @Autowired
    private ProductDBH productDBH;

    @BeforeAll
    public void setUp() {
        super.setUp();
        Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
        Product product2 = new Product("Iphone1","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);

        Role memberRole = new Role(userRoles.ROLE_MEMBER);
        memberRole.setId(1);
        Set<Role> roles = new HashSet<>();
        roles.add(memberRole);
        Member testMember = new Member("Rose",
                "Nyland",
                "user",
                "test",
                "SaintOlafinrerere@gmail.com",
                LocalDate.of(1980,4,5),
                "USA",
                "SaintOlaf",
                "UrlOfPic",
                roles,
                LocalDate.now(),
                true );
        product.setSeller(testMember.getUsername());
        testProduct = product;
        productDBH.save(product);
        productDBH.save(product2);
//        memberDBH.save(testMember);
    }

    @Test
    public void getByIdTest() throws Exception{
        // arrange
        String endpoint = "http://locahost:8080/api/product/GetId/1";
        // act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
        // assert
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Product returnedProduct = super.mapFromJson(content, Product.class);
        assertNotNull(returnedProduct);

        //test
    } //can add if its not found

    @Test
    public void getAllProductsTest() throws Exception{
        String endpoint = "http://locahost:8080/api/product/getAll";
        //act
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200,status);
// Add if the product size is known
//        String content = mvcResult.getResponse().getContentAsString();
//        List<Product> products = Arrays.asList(super.mapFromJson(content,Product[].class));
//        assertEquals(1, products.size());
    }

    @Test
    public void getProductByNameTest() throws Exception {
//        String endpoint = "http://locahost:8080/api/product/Iphone";
//        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
//        int status = mvcResult.getResponse().getStatus();
//        assertEquals(200,status);
//        String content = mvcResult.getResponse().getContentAsString();
//        Product product = super.mapFromJson(content,Product.class);
//        assertNotNull(product);
    }

    @Test
    public void getAllCategories() throws Exception {
        String endpoint = "http://locahost:8080/api/product/getAllCategory";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200,status);
        String content = mvcResult.getResponse().getContentAsString();
        List<CategoryType> listOfCategories = Arrays.asList(super.mapFromJson(content,CategoryType[].class));
        assertEquals(11,listOfCategories.size());
    }

    @Test
    public void createProductTest() throws Exception {
        String endpoint = "http://locahost:8080/api/product";
        AdvertiseRequest adv = new AdvertiseRequest();
        testProduct.setCreationDate(null);
        adv.setProduct(testProduct);
        adv.setSellerUsername("user");
        String testProdRequestJson = super.mapToJson(adv);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(endpoint).contentType(MediaType.APPLICATION_JSON_VALUE).content(testProdRequestJson).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(201,status);
        String content = mvcResult.getResponse().getContentAsString();
        Product product = super.mapFromJson(content,Product.class);
        assertNotNull(product);
    }

    @Test
    public void deleteProductTest() throws Exception {
        String endpoint = "http://locahost:8080/api/product/removeProduct/2";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(endpoint).contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200,status);

    }
    @Test
    public void updateProductTest() throws Exception {
        String endpoint = "http://locahost:8080/api/product/updateProduct/1";
        testProduct.setCreationDate(null);
        String testProdRequestJson = super.mapToJson(testProduct);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(endpoint).contentType(MediaType.APPLICATION_JSON_VALUE).content(testProdRequestJson).accept(MediaType.APPLICATION_JSON)).andReturn();
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200,status);
        String content = mvcResult.getResponse().getContentAsString();
        Product product = super.mapFromJson(content,Product.class);
        assertNotNull(product);
    }

}
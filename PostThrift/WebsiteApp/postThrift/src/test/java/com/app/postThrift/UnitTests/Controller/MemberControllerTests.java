package com.app.postThrift.UnitTests.Controller;

import com.app.postThrift.IntegrationTest.AbstractTest;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.models.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class MemberControllerTests extends AbstractTest {
    private Product testProduct;
    private Member testMember;
    @Autowired
    private MemberDBH memberDBH;
    @Autowired
    private ProductDBH productDBH;
    @Override
    @BeforeAll
    public void setUp(){
        super.setUp();
        Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);

        Role memberRole = new Role(userRoles.ROLE_MEMBER);
        memberRole.setId(1);
        Set<Role> roles = new HashSet<>();
        roles.add(memberRole);
        Member testMember = new Member(
                "Rose",
                "Nyland",
                "user",
                "test",
                "SaintOlaf@gmail.com",
                LocalDate.of(1980,4,5),
                "USA",
                "SaintOlaf",
                "UrlOfPic",
                roles,
                LocalDate.now(),
                true );
        product.setSeller(testMember.getUsername());
        testProduct = product;
        productDBH.save(product);
        memberDBH.save(testMember);
    }

    
    @Test
    @WithMockUser(roles = {"MEMBER"})
    public void getByIdTest() throws Exception{
        // arrange
        String endpoint = "http://locahost:8080/api/member/getById/1";
        // act
        testProduct.setCreationDate(null);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(endpoint).accept(MediaType.APPLICATION_JSON)).andReturn();
        // assert
        int status = mvcResult.getResponse ().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Member returnedProduct = super.mapFromJson(content, Member.class);
        assertNotNull(returnedProduct);

        //test
    }

}

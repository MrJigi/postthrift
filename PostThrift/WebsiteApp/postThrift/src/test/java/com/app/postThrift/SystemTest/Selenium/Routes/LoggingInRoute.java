package com.app.postThrift.SystemTest.Selenium.Routes;

import com.app.postThrift.PostThriftApplication;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.models.Member;
import com.app.postThrift.models.Role;
import com.app.postThrift.models.userRoles;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.util.*;

@RunWith(SpringRunner.class)
@TestPropertySource(locations = "classpath:application-test.properties")
@DirtiesContext(classMode =  DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class LoggingInRoute {

    @Autowired
    PostThriftApplication postThriftApplication;
    @Autowired
    private MemberDBH memberDBH;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private ProductDBH productDBH;


    @Before
    public void setUp(){
        Role memberRole = new Role();
        memberRole.setId(1);
        memberRole.setName(userRoles.ROLE_MEMBER);
        Set<Role> roles = new Set<Role>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Role> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Role role) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Role> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
        roles.add(memberRole);
        Member testUser = new Member(
                "Garry",
                "Browinski",
                "Garryson",
                passwordEncoder.encode("testpassword"),
                "rightemail@gmail.com"
                , LocalDate.of(1999,6,20),
                "Netherlands",
                "Eindhoven",
                ""
                , roles,LocalDate.now(),true);
        testUser.setRoles(roles);
        memberDBH.save(testUser);
        //
        Role adminRole = new Role();
        adminRole.setId(1);
        adminRole.setName(userRoles.ROLE_MEMBER);
        Set<Role> adminSetRoles = new Set<Role>() {
            @Override
            public int size() {
                return 0;
            }

            @Override
            public boolean isEmpty() {
                return false;
            }

            @Override
            public boolean contains(Object o) {
                return false;
            }

            @Override
            public Iterator<Role> iterator() {
                return null;
            }

            @Override
            public Object[] toArray() {
                return new Object[0];
            }

            @Override
            public <T> T[] toArray(T[] a) {
                return null;
            }

            @Override
            public boolean add(Role role) {
                return false;
            }

            @Override
            public boolean remove(Object o) {
                return false;
            }

            @Override
            public boolean containsAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean addAll(Collection<? extends Role> c) {
                return false;
            }

            @Override
            public boolean retainAll(Collection<?> c) {
                return false;
            }

            @Override
            public boolean removeAll(Collection<?> c) {
                return false;
            }

            @Override
            public void clear() {

            }
        };
        roles.add(adminRole);
        Member testAdmin = new Member(
                "Garry",
                "Browinski",
                "Garryson",
                passwordEncoder.encode("testpassword"),
                "rightemail@gmail.com"
                , LocalDate.of(1999,6,20),
                "Netherlands",
                "Eindhoven",
                ""
                , roles,LocalDate.now(),true);
        testUser.setRoles(roles);
        memberDBH.save(testUser);
    }



}



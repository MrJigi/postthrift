package com.app.postThrift.SystemTest.Selenium.Authorization.LogIn;

import com.app.postThrift.PostThriftApplication;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class AuthLogInTest {

@Before
public void setUp(){


}


    @Test
    public void LogInTest() throws Exception {
        //arrange
        AuthLogInPage logInPage = new AuthLogInPage();
        Thread.sleep(2000);
        //act
        boolean result = logInPage.checkIfTextBoxIsThere();
        assertTrue(result);
       // boolean logInResult = logInPage.logInTest("user","password");
        Thread.sleep(1000);
        //assert
        logInPage.logInTest("user","password");
       // logInPage.submitLogInForm();


        logInPage.closeBrowser();

    }

}

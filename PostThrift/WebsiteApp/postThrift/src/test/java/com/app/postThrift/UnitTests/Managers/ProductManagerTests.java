package com.app.postThrift.UnitTests.Managers;

import com.app.postThrift.Managers.ProductManager;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.exceptions.InvalidDataException;
import com.app.postThrift.exceptions.ResourceNotFoundException;
import com.app.postThrift.models.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.TestPropertySource;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestPropertySource(locations = "classpath:application-test.properties")
public class ProductManagerTests {
    private Member testMember;
    @Mock
    ProductDBH productDbh;
    @Mock
    MemberDBH memberDBH;
    @InjectMocks
    ProductManager productManager ;
    Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);

    @BeforeAll
    public void setUp(){
        MockitoAnnotations.openMocks(this);
        product.setId(1);
        Role memberRole = new Role(userRoles.ROLE_MEMBER);
        Set<Role> roles = new HashSet<>();
        roles.add(memberRole);
        Member testMember = new Member("Rose",
                "Nyland",
                "user",
                "test",
                "SaintOlaf@gmail.com",
                LocalDate.of(1980,4,5),
                "USA",
                "SaintOlaf",
                "UrlOfPic",
                roles,
                LocalDate.now(),
                true );
        product.setSeller(testMember.getUsername());
        this.testMember = testMember;
    }

    @Test
    List<Product> productList(){
        List<Product> productList= productList = new ArrayList<>();
        productList.add(new Product("Laptop","260","Haven't used it in a while because i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics));
        productList.add(new Product("Book","6","Finished my courses so selling it.", LocalDate.now(),"Rotterdam", CategoryType.Entertainment));
        return productList;
    }
    @Test
    void testIfExistsTest() { //getProductByName
        //Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
        Mockito.when(productDbh.getByName("Iphone")).thenReturn(product);
        Product testProduct = productManager.testIfExists("Iphone");

        assertEquals(testProduct,product);
    }

    @Test
    void testIfExistsWithNoProductTest(){
        // arrange
        Mockito.when(productDbh.getByName("Iphone")).thenReturn(null);
        // act
        Product testProduct = productManager.testIfExists("Iphone");
        // assert
        assertNull(testProduct);
        }

    @Test
    void getAllCategoryTest() throws Exception{
        // arrange

        // act
        List<CategoryType> returnedCategories = productManager.getAllCategory();
        // assert
        int categoryNr = returnedCategories.size();
        assertEquals(11, categoryNr);
    }

    @Test
    void createProductWithInvalidProductTest() throws Exception{
        Exception exception = assertThrows(InvalidDataException.class, () -> {
            Product createdProduct = productManager.createProduct(null,testMember.getUsername());
        });
    }

    @Test
    void createProductWithNoMemberFoundTest() throws Exception{
        Mockito.when(memberDBH.getByUsername("user")).thenReturn(null);
        Exception exception = assertThrows(ResourceNotFoundException.class, () -> {
            Product createdProduct = productManager.createProduct(product,testMember.getUsername());
        });
    }

    @Test
    void createProductTest() throws Exception{
        Mockito.when(memberDBH.getByUsername("user")).thenReturn(testMember);

        Product createdProduct = productManager.createProduct(product,testMember.getUsername());

        assertNotNull(createdProduct);
    }

    @Test
    void getProductById(){
        //Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);
        //arrange
        Mockito.when(productDbh.getById(1)).thenReturn(product);
        //act
        Product testProduct = productManager.getProductById(1);
        //assert
        assertEquals(testProduct,product);
    }


    @Test
    void updateProduct(){
        Product newProduct = productList().get(1);
        newProduct.setId(2);

        Mockito.when(productDbh.getById(1)).thenReturn(product); //get product
        Product updatedProduct = productManager.updateProduct(newProduct,1);
        assertEquals(newProduct,updatedProduct);
    }

    @Test
    void deleteProductTest(){ //doesn't give null from ddb
        // arrange
        Mockito.when(productDbh.getById(1)).thenReturn(product); //get product
        // act
        boolean result = productManager.deleteProduct(product);
        // assert
        assertTrue(result);

    }

    @Test
    void deleteProductTestWithInvalidProduct(){
        //arrange
        Mockito.when(productDbh.getById(1)).thenReturn(null);
        //act
        boolean result = productManager.deleteProduct(null);
        //assert
        assertFalse(result);
    }

    @Test
    void getAllProductsTest(){
        //arrange
        Mockito.when(productManager.getAllProducts()).thenReturn(productList());
        // act
        List<Product> result = productList();
        // assert
        assertNotNull(result);
    }



}

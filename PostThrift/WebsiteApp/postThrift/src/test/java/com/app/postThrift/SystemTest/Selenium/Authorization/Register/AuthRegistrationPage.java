package com.app.postThrift.SystemTest.Selenium.Authorization.Register;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AuthRegistrationPage {


    private WebDriver driver;
    private int waitTime = 3000;
    private String url = "http://localhost:4200/#/LogInComponent";

//    static {
//        System.setProperty("webdriver.gecko.driver", "C:\\Gecko\\geckodriver.exe");
//    }


    public AuthRegistrationPage() {
        System.setProperty("webdriver.gecko.driver", "C:\\Gecko\\geckodriver.exe");
        //System.setProperty("webdriver.chrome.driver", "C:\\Chrome-Selenium-drivers\\chromedriver.exe");
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(url);


    }

    public AuthRegistrationPage(WebDriver webDriver,int waitTime) {
        this.waitTime = waitTime;
        this.driver = webDriver;
    }

    public void travelToLogInByNavbar() throws InterruptedException {
        // initialise the js executor
        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
        // scroll to top to be sure the navbar is into view
        jse.executeScript("window.scrollTo({ top: 0, behavior: 'smooth' });");
        Thread.sleep(1000);
        // click the nav link for the listing
        String jsScript = "let navCreationBtn = document.querySelector(\"ul.navbar-nav li.nav-item:nth-child(6) a\");" +
                "navCreationBtn.click();";
        jse.executeScript(jsScript);
    }

    public boolean checkIfTextBoxIsThere(){
        String usernameBox = "username";
        String passwordBox = "password";
        try {

            WebElement FirstName = driver.findElement(By.id("firstName"));
            WebElement LastName = driver.findElement(By.id("lastName"));
            WebElement Username = driver.findElement(By.id("registrationUsername"));
            WebElement Password = driver.findElement(By.id("registrationPassword"));
            WebElement PasswordRepeat = driver.findElement(By.id("passwordMatch"));
            WebElement Email = driver.findElement(By.id("email"));

            return true;
        }
        catch (NoSuchElementException e){
            return false;
        }
    }

    public void changeToRegister() throws InterruptedException {

        // create the javaScript executor
        JavascriptExecutor jse = ((JavascriptExecutor) this.driver);
        //get inputs and add text
        // name
        WebElement registrationSwitch = driver.findElement(By.id("pills-profile-tab"));
        registrationSwitch.click();
        Thread.sleep(500);
//        if (registrationSwitch.isSelected())
//        {
//            return true;
//        }
//        else
//        {
//            return false;
//
//        }


        // price
    }

    public void registrationTest(String firstName, String lastName,String Username, String Password,String email) throws InterruptedException {

        // create the javaScript executor
        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
        //get inputs and add text
        // name
        WebElement nameInput = driver.findElement(By.id("firstName"));
        nameInput.sendKeys(firstName);
        Thread.sleep(500);
        // price
        WebElement lastNameInput = driver.findElement(By.id("lastName"));
        lastNameInput.sendKeys(lastName);
        Thread.sleep(500);

        WebElement registrationUsernameInput = driver.findElement(By.id("registrationUsername"));
        registrationUsernameInput.sendKeys(Username);
        Thread.sleep(500);
        // price
        WebElement registrationPasswordInput = driver.findElement(By.id("registrationPassword"));
        registrationPasswordInput.sendKeys(Password);
        Thread.sleep(500);

        WebElement passwordMatchInput = driver.findElement(By.id("passwordMatch"));
        passwordMatchInput.sendKeys(Password);
        Thread.sleep(500);
        // price
        WebElement emailInput = driver.findElement(By.id("email"));
        emailInput.sendKeys(email);
        Thread.sleep(500);

        WebElement submit = driver.findElement(By.id("registrationSubmit"));
        submit.submit();
        Thread.sleep(5000);
        //Thread.sleep(500);

    }

    public void submitLogInForm(){
        // create the javaScript executor
        JavascriptExecutor jse = ((JavascriptExecutor)this.driver);
        // writing js script to get the select option and click it
        String jsScript = "let logIn-submit = document.querySelector(\"div.list-group-horizontal form button[type='submit']\");" +
                "logInBtn.click();";
        jse.executeScript(jsScript);
    }





    public AuthRegistrationPage(WebDriver driver) {
        this.driver = driver;
    }
    public void closeWindow() { this.driver.close();}
    public void closeBrowser() {this.driver.quit();}

    public int getWaitTime() {
        return waitTime;
    }
    public void setWaitTime(int waitTime) {
        this.waitTime = waitTime;
    }
}

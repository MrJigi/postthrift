package com.app.postThrift.UnitTests.Controller;

import com.app.postThrift.IntegrationTest.AbstractTest;
import com.app.postThrift.Repo.MemberDBH;
import com.app.postThrift.Repo.ProductDBH;
import com.app.postThrift.models.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;import static org.junit.jupiter.api.Assertions.assertEquals;import static org.junit.jupiter.api.Assertions.assertNotNull;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AuthControllerTests extends AbstractTest {
    private Product testProduct ;
    @Autowired
    private MemberDBH memberDBH;
    @Autowired
    private ProductDBH productDBH;
    @Override
    @BeforeAll

    public void setUp(){
        super.setUp();
        Product product = new Product("Iphone","80","Haven't used it in a while becouse i bought a new one so selling this one.", LocalDate.now(),"Eindhoven", CategoryType.Electronics);

        Role memberRole = new Role(userRoles.ROLE_MEMBER);
        memberRole.setId(1);
        Set<Role> roles = new HashSet<>();
        roles.add(memberRole);
        Member testMember = new Member("Rose",
                "Nyland",
                "user",
                "test",
                "SaintOlaffffff@gmail.com",
                LocalDate.of(1980,4,5),
                "USA",
                "SaintOlaf",
                "UrlOfPic",
                roles,
                LocalDate.now(),
                true );
        product.setSeller(testMember.getUsername());
        testProduct = product;
        productDBH.save(product);
        memberDBH.save(testMember);
    }

    @Test
    public void registerUserTest() throws Exception{


            String endpoint = "http://locahost:8080/api/auth/signup";
            AdvertiseRequest adv = new AdvertiseRequest();
            testProduct.setCreationDate(null);
            adv.setProduct(testProduct);
            adv.setSellerUsername("user");
            String testProdRequestJson = super.mapToJson(adv);
            org.springframework.test.web.servlet.MvcResult mvcResult = mvc.perform(org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post(endpoint).contentType(org.springframework.http.MediaType.APPLICATION_JSON_VALUE).content(testProdRequestJson).accept(org.springframework.http.MediaType.APPLICATION_JSON)).andReturn();
            int status = mvcResult.getResponse().getStatus();
            assertEquals(201,status);
            String content = mvcResult.getResponse().getContentAsString();
            Product product = super.mapFromJson(content,Product.class);
            assertNotNull(product);

    }

}
